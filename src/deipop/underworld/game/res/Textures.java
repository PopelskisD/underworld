package deipop.underworld.game.res;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;

/**
 * @author Deividas Popelskis <deividas.popelskis@gmail.com>
 */
public class Textures {

	/**
	 * The path, which contains all the spritesheets.
	 */
	public static final String SPRITESHEET_PATH = "res/spritesheet/";
	
	/**
	 * The player textures.
	 */
	private static List<AtlasRegion> playerTextures = new ArrayList<>();
	
	/**
	 * The monster textures.
	 */
	private static List<AtlasRegion> monsterTextures = new ArrayList<>();
	
	/**
	 * The item textures.
	 */
	private static List<AtlasRegion> itemTextures = new ArrayList<>();
	
	/**
	 * The object textures.
	 */
	private static List<AtlasRegion> objectTextures = new ArrayList<>();
	
	/**
	 * The heads-up display textures.
	 */
	private static List<AtlasRegion> hudTextures = new ArrayList<>();
	
	/**
	 * Loads all the textures on application startup.
	 */
	static {
		loadTextures("player", playerTextures);
		loadTextures("monster", monsterTextures);
		loadTextures("item", itemTextures);
		loadTextures("object", objectTextures);
		loadTextures("hud", hudTextures);
	}
	
	/**
	 * Loads a texture pack.
	 * @param packName - the pack name.
	 * @param array - the array, which has to be filled with the loaded textures.
	 */
	private static void loadTextures(String packName, List<AtlasRegion> array) {
		TextureAtlas atlas = new TextureAtlas(Gdx.files.internal(SPRITESHEET_PATH + packName + ".pack"));
		
		for (AtlasRegion region : atlas.getRegions()) {
			array.add(region);
		}
	}
	
	/**
	 * Gets a texture.
	 * @param name - the name of the texture.
	 * @param array - the array which contains the texture.
	 * @return a specified texture.
	 */
	public static TextureRegion getTexture(String name, List<AtlasRegion> array) {
		for (AtlasRegion region : array) {
			if (region.name.contains(name)) {
				return region;
			}
		}
		return null;
	}
	
	/**
	 * Gets a player texture.
	 * @param name - the name of the player texture.
	 * @return a specified player texture.
	 */
	public static TextureRegion getPlayerTexture(String name) {
		return getTexture(name, playerTextures);
	}
	
	/**
	 * Gets a monster texture.
	 * @param name - the name of the monster texture.
	 * @return a specified monster texture.
	 */
	public static TextureRegion getMonsterTexture(String name) {
		return getTexture(name, playerTextures);
	}
	
	/**
	 * Gets an item texture.
	 * @param name - the name of the item texture.
	 * @return a specified item texture.
	 */
	public static TextureRegion getItemTexture(String name) {
		return getTexture(name, itemTextures);
	}
	
	/**
	 * Gets an object texture.
	 * @param name - the name of the object texture.
	 * @return a specified object texture.
	 */
	public static TextureRegion getObjectTexture(String name) {
		return getTexture(name, objectTextures);
	}
	
	/**
	 * Gets a heads-up display texture.
	 * @param name - the name of the heads-up display texture.
	 * @return a specified heads-up display texture.
	 */
	public static TextureRegion getHUDTexture(String name) {
		return getTexture(name, hudTextures);
	}
	
	/**
	 * Gets the player walking textures.
	 * @return the player walking textures.
	 */
	public static TextureRegion[] getPlayerWalkTextures() {
		List<AtlasRegion> walkTextures = new ArrayList<>();
		
		for (AtlasRegion region : playerTextures) {
			if (region.name.contains("walk")) {
				walkTextures.add(region);
			}
		}
		
		List<AtlasRegion> walkTexturesCopy = new ArrayList<>(walkTextures);
		Collections.reverse(walkTexturesCopy);
		
		return walkTextures.stream().toArray(TextureRegion[]::new);
	}
	
	/**
	 * Gets the monster walking textures.
	 * @param monsterName - the name of the monster.
	 * @return the monster walking textures.
	 */
	public static TextureRegion[] getMonsterWalkTextures(String monsterName) {
		List<AtlasRegion> walkTextures = new ArrayList<>();
		
		for (AtlasRegion region : monsterTextures) {
			if (region.name.contains(monsterName + "Walk")) {
				walkTextures.add(region);
			}
		}
		
		return walkTextures.stream().toArray(TextureRegion[]::new);
	}
	
}