package deipop.underworld.game.res;

import java.util.HashMap;
import java.util.Map;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.files.FileHandle;

import deipop.underworld.game.Settings;

/**
 * @author Deividas Popelskis <deividas.popelskis@gmail.com>
 */
public class Sounds {
	
	/**
	 * The path, which contains all the sounds.
	 */
	public static final String SOUND_PATH = "res/sound/";

	/**
	 * The loaded sounds.
	 */
	private static Map<String, Sound> sounds = new HashMap<>();
	
	/**
	 * Loads all the sounds on application startup.
	 */
	static {
		load();
	}
	
	/**
	 * Loads all the sounds.
	 */
	private static void load() {
		for (FileHandle file : Gdx.files.internal(SOUND_PATH).list()) {
			String name = file.name().substring(0, file.name().indexOf("."));
			
			Sound sound = Gdx.audio.newSound(Gdx.files.internal(file.path()));
			
			sounds.put(name, sound);
		}
	}
	
	/**
	 * Plays a sound.
	 * @param name - the sound name.
	 */
	public static void play(String name) {
		if (Settings.getVolume().equalsIgnoreCase("off")) {
			return;
		}
		
		sounds.get(name).play(Settings.getVolume().equalsIgnoreCase("med") ? 0.5f : 1f);
	}
	
}