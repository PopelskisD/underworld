package deipop.underworld.game.res;

import java.util.HashMap;
import java.util.Map;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

/**
 * @author Deividas Popelskis <deividas.popelskis@gmail.com>
 */
public class Skins {

	/**
	 * The path, which contains all the skins.
	 */
	public static final String SKIN_PATH = "res/skin/";

	/**
	 * The loaded skins.
	 */
	private static Map<String, Skin> skins = new HashMap<>();
	
	/**
	 * Loads all the skins on application startup.
	 */
	static {
		load();
	}
	
	/**
	 * Loads all the skins.
	 */
	public static void load() {
		for (FileHandle file : Gdx.files.internal(SKIN_PATH).list()) {
			if (file.name().endsWith(".fnt") || file.name().endsWith(".png")) {
				continue;
			}
			
			String name = file.name().substring(0, file.name().indexOf("."));
			
			TextureAtlas atlas = new TextureAtlas(Gdx.files.internal(file.pathWithoutExtension() + ".pack"));
			Skin skin = new Skin();
			skin.addRegions(atlas);
			skin.load(Gdx.files.internal(file.pathWithoutExtension() + ".json"));	
			
			skins.put(name, skin);
		}
	}
	
	/**
	 * Gets the global skin.
	 * @return the global skin.
	 */
	public static Skin getGlobal() {
		return skins.get("global");
	}
	
}