package deipop.underworld.game.res;

import java.util.HashMap;
import java.util.Map;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;

/**
 * @author Deividas Popelskis <deividas.popelskis@gmail.com>
 */
public class Maps {
	
	/**
	 * The path, which contains all the map files.
	 */
	public static final String MAP_PATH = "res/map/";
	
	/**
	 * The loaded maps.
	 */
	private static Map<Integer, TiledMap> maps = new HashMap<>();
	
	/**
	 * Loads all the maps on application startup.
	 */
	static {
		load();
	}
	
	/**
	 * Loads all the maps.
	 */
	private static void load() {
		for (FileHandle file : Gdx.files.internal(MAP_PATH).list()) {
			if (!file.name().endsWith(".tmx")) {
				continue;
			}
			
			int underscoreIndex = file.name().indexOf("_");
			int dotIndex = file.name().indexOf(".");
			int level = Integer.parseInt(file.name().substring(underscoreIndex + 1, dotIndex));
			
			TiledMap map = new TmxMapLoader().load(file.path());
			maps.put(level, map);
		}
	}
	
	/**
	 * Gets the map of a specific level.
	 * @param level - the map level.
	 * @return specified level map.
	 */
	public static TiledMap getMap(int level) {
		return maps.get(level);
	}
	
	/**
	 * Gets the map count.
	 * @return the map count.
	 */
	public static int getMapCount() {
		return maps.size();
	}
	
}