package deipop.underworld.game.constant;

/**
 * @author Deividas Popelskis <deividas.popelskis@gmail.com>
 */
public class Box2D {
	
	/**
	 * The gravity.
	 */
	public static final float GRAVITY = -9.8f;
	
	/**
	 * Pixels per meter.
	 */
	public static final float PPM = 100f;
	
	/**
	 * The player bits.
	 */
	public static final int BIT_PLAYER = 1;
	
	/**
	 * The ground tile bits.
	 */
	public static final int BIT_GROUND_TILE = 2;
	
	/**
	 * The water tile bits.
	 */
	public static final int BIT_WATER_TILE = 4;
	
	/**
	 * The object bits.
	 */
	public static final int BIT_OBJECT_TILE = 8;
	
	/**
	 * The door bits.
	 */
	public static final int BIT_DOOR = 16;
	
	/**
	 * The coin bits.
	 */
	public static final int BIT_COIN = 32;
	
	/**
	 * The key bits.
	 */
	public static final int BIT_KEY = 64;
	
	/**
	 * The monster bits.
	 */
	public static final int BIT_MONSTER = 128;
	
}