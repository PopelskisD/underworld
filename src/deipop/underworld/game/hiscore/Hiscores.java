package deipop.underworld.game.hiscore;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import deipop.underworld.game.Settings;
import deipop.underworld.game.level.entity.player.Player;

/**
 * @author Deividas Popelskis <deividas.popelskis@gmail.com>
 */
public class Hiscores {
	
	/**
	 * The hiscores file path.
	 */
	public static final String FILE_PATH = System.getProperty("user.home") + "\\hiscores.uw";
	
	/**
	 * The maximum amount of scores.
	 */
	public static final int MAX_SCORES = 10;
	
	/**
	 * The scores.
	 */
	private static final List<Score> scores = new ArrayList<>(MAX_SCORES);
	
	/**
	 * Loads the highscores.
	 */
	public static void load() {
		scores.clear();
		
		File file = new File(FILE_PATH);
		
		try {
			if (file.exists()) {
				DataInputStream inputStream = new DataInputStream(new FileInputStream(file));
				
				while (inputStream.available() > 0) {
					String name = inputStream.readUTF();
					int score = inputStream.readInt();
					
					scores.add(new Score(name, score));
				}
				
				inputStream.close();
			} else {
				file.createNewFile();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Saves the score (if the score is in top 10).
	 * @param player - the player which highscore needs to be saved.
	 */
	public static void save(Player player) {
		load();
		
		Score score = new Score(Settings.getName(), player.getPoints());
		
		if (scores.size() > 9) {
			int indexToAdd = -1;
			
			for (int i = scores.size() - 1; i > -1; i--) {
				if (!scores.get(i).compare(score)) {
					indexToAdd = i;
				}
			}
			
			if (indexToAdd != -1) {
				scores.add(indexToAdd, score);
				scores.remove(10);
			}
		} else {
			boolean added = false;
			
			for (int i = 0; i < scores.size(); i++) {
				if (!scores.get(i).compare(score)) {
					scores.add(i, score);
					added = true;
					break;
				}
			}
			
			if (!added) {
				scores.add(score);
			}
		}

		try {
			DataOutputStream outputStream = new DataOutputStream(new FileOutputStream(FILE_PATH));
			
			for (Score s : scores) {
				outputStream.writeUTF(s.getPlayerName());
				outputStream.writeInt(s.getScore());
			}
			
			outputStream.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Gets the scores.
	 * @return the scores.
	 */
	public static List<Score> getScores() {
		return scores;
	}
	
}