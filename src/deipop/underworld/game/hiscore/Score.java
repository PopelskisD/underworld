package deipop.underworld.game.hiscore;

/**
 * @author Deividas Popelskis <deividas.popelskis@gmail.com>
 */
public class Score {

	/**
	 * The player name.
	 */
	private final String playerName;
	
	/**
	 * The score.
	 */
	private int score;
	
	/**
	 * Creates a new hiscore.
	 * @param playerName - the player's name, which this score belongs to.
	 * @param score - the score.
	 */
	public Score(String playerName, int score) {
		this.playerName = playerName;
		this.score = score;
	}
	
	/**
	 * Compares this hiscore with another hiscore.
	 * @param hiscore - the 
	 * @return true - if this hiscore is better, false - if the other hiscore is better or equal.
	 */
	public boolean compare(Score hiscore) {
		return getScore() > hiscore.getScore();
	}
	
	/**
	 * Gets the player name.
	 * @return the player name.
	 */
	public String getPlayerName() {
		return playerName;
	}
	
	/**
	 * Gets the score.
	 * @return the score.
	 */
	public int getScore() {
		return score;
	}
	
}