package deipop.underworld.game;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * @author Deividas Popelskis <deividas.popelskis@gmail.com>
 */
public class Settings {

	/**
	 * The path to the settings file.
	 */
	public static final String FILE_PATH = System.getProperty("user.home") + "\\settings.uw";
	
	/**
	 * The settings.
	 */
	private static final Properties settings = new Properties();
	
	/**
	 * Loads the settings on application startup.
	 */
	static {
		load();
	}
	
	/**
	 * Loads the saved settings.
	 */
	public static void load() {
		try {
			if (!new File(FILE_PATH).exists()) {
				settings.put("name", "Player");
				settings.put("volume", "high");
				
				save();
			} else {
				settings.load(new FileInputStream(FILE_PATH));
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Saves the settings.
	 */
	private static void save() {
		try {
			FileOutputStream outputStream = new FileOutputStream(FILE_PATH);
			
			settings.store(outputStream, "");
			
			outputStream.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Sets the player name.
	 * @param name - the player name.
	 */
	public static void setName(String name) {
		settings.put("name", name);
		
		save();
	}
	
	/**
	 * Gets the player name.
	 * @return the player name.
	 */
	public static String getName() {
		return settings.getProperty("name");
	}
	
	/**
	 * Sets the volume.
	 * @param volume - the sound volume.
	 */
	public static void setVolume(String volume) {
		settings.put("volume", volume);
		
		save();
	}
	
	/**
	 * Gets the sound volume.
	 * @return the volume.
	 */
	public static String getVolume() {
		return settings.getProperty("volume");
	}
	
}