package deipop.underworld.game;

import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.TweenManager;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Files.FileType;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.badlogic.gdx.graphics.GL30;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Actor;

import deipop.underworld.game.screen.Screen;
import deipop.underworld.game.screen.ScreenManager;
import deipop.underworld.game.util.ActorAccessor;

/**
 * @author Deividas Popelskis <deividas.popelskis@gmail.com>
 */
public class Application extends ApplicationAdapter {
	
	/**
	 * The name of the application.
	 */
	public static final String NAME = "Underworld";
	
	/**
	 * The width of the application.
	 */
	public static final int WIDTH = 900;
	
	/**
	 * The height of the application.
	 */
	public static final int HEIGHT = 600;
	
	/**
	 * The application debug flag.
	 */
	public static final boolean DEBUG = false;
	
	/**
	 * The sprite batch.
	 */
	private static SpriteBatch spriteBatch;
	
	/**
	 * The tween manager.
	 */
	private static TweenManager tweenManager;
	
	/**
	 * The screen manager.
	 */
	private static ScreenManager screenManager;
	
	/**
	 * The application starting point.
	 * @param arguments - the command line arguments.
	 */
	public static void main(String[] arguments) {
		LwjglApplicationConfiguration configuration = new LwjglApplicationConfiguration();
		configuration.title = NAME;
		configuration.width = WIDTH;
		configuration.height = HEIGHT;
		configuration.resizable = false;
		configuration.addIcon("res/icon.png", FileType.Internal);
		
		new LwjglApplication(new Application(), configuration);
	}
	
	@Override
	public void create() {
		Tween.registerAccessor(Actor.class, new ActorAccessor());
		
		spriteBatch = new SpriteBatch();
		tweenManager = new TweenManager();
		screenManager = new ScreenManager();
	}
	
	@Override
	public void render() {
		if (DEBUG) {
			Gdx.graphics.setTitle(NAME + " FPS: " + Gdx.graphics.getFramesPerSecond());
		}
		
		Gdx.gl.glClear(GL30.GL_COLOR_BUFFER_BIT);
		
		screenManager.handleInput();
		screenManager.update(Gdx.graphics.getDeltaTime());
		screenManager.render();
	}
	
	@Override
	public void dispose() {
		spriteBatch.dispose();
		tweenManager.killAll();
		screenManager.dispose();
	}
	
	/**
	 * Gets the sprite batch.
	 * @return the sprite batch.
	 */
	public static SpriteBatch getSpriteBatch() {
		return spriteBatch;
	}
	
	/**
	 * Gets the tween manager.
	 * @return the tween manager.
	 */
	public static TweenManager getTweenManager() {
		return tweenManager;
	}
	
	/**
	 * Sets the current screen.
	 * @param screen - the screen to set to.
	 */
	public static void setScreen(Screen screen) {
		screenManager.setScreen(screen);
	}
	
}