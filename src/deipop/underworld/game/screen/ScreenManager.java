package deipop.underworld.game.screen;

import deipop.underworld.game.Application;
import deipop.underworld.game.screen.impl.GameScreen;
import deipop.underworld.game.screen.impl.TitleScreen;

/**
 * @author Deividas Popelskis <deividas.popelskis@gmail.com>
 */
public class ScreenManager {
	
	/**
	 * The current screen.
	 */
	public Screen screen;
	
	/**
	 * Creates a new screen manager.
	 */
	public ScreenManager() {
		setScreen(Application.DEBUG ? new GameScreen(1) : new TitleScreen());
	}
	
	/**
	 * Handles the input of the current screen.
	 */
	public void handleInput() {
		screen.handleInput();
	}
	
	/**
	 * Updates the current screen.
	 * @param deltaTime - the delta time.
	 */
	public void update(float deltaTime) {
		screen.update(deltaTime);
	}
	
	/**
	 * Renders the current screen.
	 */
	public void render() {
		screen.render();
	}
	
	/**
	 * Disposes the current screen.
	 */
	public void dispose() {
		if (screen != null) {
			screen.dispose();
		}
	}
	
	/**
	 * Sets the current screen.
	 * @param screen - the screen to set to.
	 */
	public void setScreen(Screen screen) {
		dispose();
		
		this.screen = screen;
		
		screen.create();
	}
	
}