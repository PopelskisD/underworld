package deipop.underworld.game.screen.impl;

import aurelienribon.tweenengine.BaseTween;
import aurelienribon.tweenengine.Timeline;
import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.TweenCallback;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import deipop.underworld.game.Application;
import deipop.underworld.game.res.Skins;
import deipop.underworld.game.screen.Screen;
import deipop.underworld.game.util.ActorAccessor;
import deipop.underworld.game.util.Scene2D;

/**
 * @author Deividas Popelskis <deividas.popelskis@gmail.com>
 */
public class MainMenuScreen extends Screen {
	
	/**
	 * The stage.
	 */
	private Stage stage;
	
	/**
	 * The background image.
	 */
	private Image background;
	
	/**
	 * The option box image.
	 */
	private Image optionBox;
	
	/**
	 * The play button.
	 */
	private TextButton playButton;
	
	/**
	 * The highscores button.
	 */
	private TextButton hiscoresButton;
	
	/**
	 * The options button.
	 */
	private TextButton optionsButton;
	
	/**
	 * The quit button.
	 */
	private TextButton quitButton;
	
	/**
	 * Plays a transition animation.
	 * @param callback - the tween callback.
	 * @return the timeline.
	 */
	private Timeline transition(TweenCallback callback) {
		return Timeline.createParallel().
				push(Tween.to(background, ActorAccessor.ALPHA, 1f).target(0f)).
				push(Tween.to(optionBox, ActorAccessor.ALPHA, 1f).target(0f)).
				push(Tween.to(playButton, ActorAccessor.POSITION_Y, 1f).target(Scene2D.getHideBottomY(playButton))).
				push(Tween.to(hiscoresButton, ActorAccessor.POSITION_Y, 1f).target(Scene2D.getHideBottomY(hiscoresButton))).
				push(Tween.to(optionsButton, ActorAccessor.POSITION_Y, 1f).target(Scene2D.getHideBottomY(optionsButton))).
				push(Tween.to(quitButton, ActorAccessor.POSITION_Y, 1f).target(Scene2D.getHideBottomY(quitButton))).
				setCallback(callback).
				start(Application.getTweenManager());
	}
	
	@Override
	public void create() {
		stage = new Stage();
		
		/*
		 * Background.
		 */
		background = new Image(Skins.getGlobal(), "background");
		background.getColor().a = 0f;
		
		/*
		 * Option box.
		 */
		optionBox = new Image(Skins.getGlobal(), "menu_box");
		optionBox.getColor().a = 0f;
		
		/*
		 * Play button.
		 */
		playButton = new TextButton("PLAY", Skins.getGlobal(), "play");
		
		/*
		 * Hiscores button.
		 */
		hiscoresButton = new TextButton("Hiscores", Skins.getGlobal());
		
		/*
		 * Options button.
		 */
		optionsButton = new TextButton("OPTIONS", Skins.getGlobal());
		/*
		 * Quit button.
		 */
		quitButton = new TextButton("QUIT", Skins.getGlobal());
		
		/*
		 * Starting positions of actors.
		 */
		Scene2D.center(optionBox, -30);
		Scene2D.hideBottom(playButton);
		Scene2D.hideBottom(hiscoresButton);
		Scene2D.hideBottom(optionsButton);
		Scene2D.hideBottom(quitButton);
		
		stage.addActor(background);
		stage.addActor(optionBox);
		stage.addActor(playButton);
		stage.addActor(hiscoresButton);
		stage.addActor(optionsButton);
		stage.addActor(quitButton);
		
		Gdx.input.setInputProcessor(stage);
		
		/*
		 * Starting animations.
		 */
		Timeline.createParallel().
		push(Tween.to(background, ActorAccessor.ALPHA, 1f).target(1f)).
		push(Tween.to(optionBox, ActorAccessor.ALPHA, 1f).target(1f)).
		push(Tween.to(playButton, ActorAccessor.POSITION_Y, 1f).target(Scene2D.getCenterY(playButton) + 70)).
		push(Tween.to(hiscoresButton, ActorAccessor.POSITION_Y, 1f).target(Scene2D.getCenterY(hiscoresButton))).
		push(Tween.to(optionsButton, ActorAccessor.POSITION_Y, 1f).target(Scene2D.getCenterY(optionsButton) - 55)).
		push(Tween.to(quitButton, ActorAccessor.POSITION_Y, 1f).target(Scene2D.getCenterY(quitButton) - 165)).
		start(Application.getTweenManager());
		
		/*
		 * Play button click listener.
		 */
		playButton.addListener(new ClickListener() {
			
			@Override
			public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
				transition(new TweenCallback() {

					@Override
					public void onEvent(int eventType, BaseTween<?> source) {
						Application.setScreen(new GameScreen(1));
					}
					
				});
			}
			
		});
		
		/*
		 * Hiscores button click listener.
		 */
		hiscoresButton.addListener(new ClickListener() {
			
			@Override
			public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
				transition(new TweenCallback() {

					@Override
					public void onEvent(int eventType, BaseTween<?> source) {
						Application.setScreen(new HiscoresScreen());
					}
					
				});
			}
			
		});
		
		/*
		 * Options button click listener.
		 */
		optionsButton.addListener(new ClickListener() {
			
			@Override
			public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
				transition(new TweenCallback() {

					@Override
					public void onEvent(int eventType, BaseTween<?> source) {
						Application.setScreen(new OptionsScreen());
					}
					
				});
			}
			
		});
		
		/*
		 * Quit button click listener.
		 */
		quitButton.addListener(new ClickListener() {
			
			@Override
			public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
				transition(new TweenCallback() {

					@Override
					public void onEvent(int eventType, BaseTween<?> source) {
						Gdx.app.exit();
					}
					
				});
			}
			
		});
	}

	@Override
	public void update(float delta) {
		stage.act(delta);
		
		Application.getTweenManager().update(delta);
	}

	@Override
	public void render() {
		stage.draw();
	}

	@Override
	public void dispose() {
		stage.dispose();
		
		Application.getTweenManager().killAll();
	}

}