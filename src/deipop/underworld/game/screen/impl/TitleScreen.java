package deipop.underworld.game.screen.impl;

import aurelienribon.tweenengine.BaseTween;
import aurelienribon.tweenengine.Timeline;
import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.TweenCallback;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;

import deipop.underworld.game.Application;
import deipop.underworld.game.Settings;
import deipop.underworld.game.res.Skins;
import deipop.underworld.game.screen.Screen;
import deipop.underworld.game.util.ActorAccessor;
import deipop.underworld.game.util.Scene2D;

/**
 * @author Deividas Popelskis <deividas.popelskis@gmail.com>
 */
public class TitleScreen extends Screen {
	
	/**
	 * The stage.
	 */
	private Stage stage;
	
	@Override
	public void create() {
		Settings.load();
		
		stage = new Stage();
		
		/*
		 * Background.
		 */
		Image background = new Image(Skins.getGlobal(), "background");
		background.getColor().a = 0f;
		
		/*
		 * Name box.
		 */
		Image nameBox = new Image(Skins.getGlobal(), "title_box");
		nameBox.getColor().a = 0f;
		
		/*
		 * Name field.
		 */
		TextField nameField = new TextField(Settings.getName() == null ? "" : Settings.getName(), Skins.getGlobal());
		TextureRegion fieldRegion = Skins.getGlobal().getRegion("field");
		nameField.setAlignment(Align.center);
		nameField.setSize(fieldRegion.getRegionWidth(), fieldRegion.getRegionHeight());
		nameField.setMaxLength(10);
		
		if (Settings.getName() != null) {
			nameField.setCursorPosition(Settings.getName().length());
		}
		
		/*
		 * Confirm button.
		 */
		TextButton confirmButton = new TextButton("CONFIRM", Skins.getGlobal(), "confirm");
		
		/*
		 * Starting positions of actors.
		 */
		Scene2D.center(nameBox);
		Scene2D.hideBottom(nameField);
		Scene2D.hideBottom(confirmButton);
		
		stage.setKeyboardFocus(nameField);
		
		stage.addActor(background);
		stage.addActor(nameBox);
		stage.addActor(nameField);
		stage.addActor(confirmButton);
		
		Gdx.input.setInputProcessor(stage);
		
		/*
		 * Starting animations.
		 */
		Timeline.createParallel().
		push(Tween.to(background, ActorAccessor.ALPHA, 1f).target(1f)).
		push(Tween.to(nameBox, ActorAccessor.ALPHA, 1f).target(1f)).
		push(Tween.to(nameField, ActorAccessor.POSITION_Y, 1f).target(Scene2D.getCenterY(nameField) + 10)).
		push(Tween.to(confirmButton, ActorAccessor.POSITION_Y, 1f).target(Scene2D.getCenterY(confirmButton) - 60)).
		start(Application.getTweenManager());
		
		/*
		 * Confirm button click listener.
		 */
		confirmButton.addListener(new ClickListener() {

			@Override
			public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
				/*
				 * Exiting animations.
				 */
				Timeline.createParallel().beginParallel().
				push(Tween.to(background, ActorAccessor.ALPHA, 1f).target(0f)).
				push(Tween.to(nameBox, ActorAccessor.ALPHA, 1f).target(0f)).
				push(Tween.to(nameField, ActorAccessor.POSITION_Y, 1f).target(Scene2D.getHideBottomY(nameField))).
				push(Tween.to(confirmButton, ActorAccessor.POSITION_Y, 1f).target(Scene2D.getHideBottomY(confirmButton))).
				setCallback(new TweenCallback() {

					@Override
					public void onEvent(int eventType, BaseTween<?> source) {
						/*
						 * Screen change after animations.
						 */
						Application.setScreen(new MainMenuScreen());
						
						if (nameField.getText().length() == 0) {
							Settings.setName("Player");
						} else {
							Settings.setName(nameField.getText());
						}
					}
					
				}).
				start(Application.getTweenManager());
			}
			
		});
	}

	@Override
	public void update(float delta) {
		stage.act(delta);
		
		Application.getTweenManager().update(delta);
	}
	
	@Override
	public void render() {
		stage.draw();
	}

	@Override
	public void dispose() {
		stage.dispose();
		
		Application.getTweenManager().killAll();
	}

}
