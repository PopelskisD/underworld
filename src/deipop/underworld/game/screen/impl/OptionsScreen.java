package deipop.underworld.game.screen.impl;

import aurelienribon.tweenengine.BaseTween;
import aurelienribon.tweenengine.Timeline;
import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.TweenCallback;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.CheckBox;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;

import deipop.underworld.game.Application;
import deipop.underworld.game.Settings;
import deipop.underworld.game.res.Skins;
import deipop.underworld.game.screen.Screen;
import deipop.underworld.game.util.ActorAccessor;
import deipop.underworld.game.util.Scene2D;

/**
 * @author Deividas Popelskis <deividas.popelskis@gmail.com>
 */
public class OptionsScreen extends Screen {

	/**
	 * The stage.
	 */
	private Stage stage;
	
	@Override
	public void create() {
		Settings.load();
		
		stage = new Stage();
		
		/*
		 * Background.
		 */
		Image background = new Image(Skins.getGlobal(), "background");
		background.getColor().a = 0f;
		
		/*
		 * Options box.
		 */
		Image optionsBox = new Image(Skins.getGlobal(), "options_box");
		optionsBox.getColor().a = 0f;
		
		/*
		 * Name field.
		 */
		TextField nameField = new TextField(Settings.getName() == null ? "" : Settings.getName(), Skins.getGlobal());
		TextureRegion fieldRegion = Skins.getGlobal().getRegion("field");
		nameField.setAlignment(Align.center);
		nameField.setSize(fieldRegion.getRegionWidth(), fieldRegion.getRegionHeight());
		nameField.setMaxLength(10);
		
		if (Settings.getName() != null) {
			nameField.setCursorPosition(Settings.getName().length());
		}
		
		/*
		 * Off sound checkbox.
		 */
		CheckBox offSoundBox = new CheckBox("", Skins.getGlobal());
		offSoundBox.getColor().a = 0f;
		
		/*
		 * Med sound checkbox.
		 */
		CheckBox medSoundBox = new CheckBox("", Skins.getGlobal());
		medSoundBox.getColor().a = 0f;
		
		/*
		 * High sound checkbox.
		 */
		CheckBox highSoundBox = new CheckBox("", Skins.getGlobal());
		highSoundBox.getColor().a = 0f;
		
		switch (Settings.getVolume()) {
		case "off":
			offSoundBox.setChecked(true);
			break;
		case "med":
			medSoundBox.setChecked(true);
			break;
		case "high":
			highSoundBox.setChecked(true);
			break;
		}
		
		/*
		 * Confirm button.
		 */
		TextButton confirmButton = new TextButton("CONFIRM", Skins.getGlobal(), "confirm");
		
		/*
		 * Starting positions of actors.
		 */
		Scene2D.center(optionsBox);
		Scene2D.hideBottom(nameField);
		Scene2D.center(offSoundBox, -115, -80);
		Scene2D.center(medSoundBox, -80);
		Scene2D.center(highSoundBox, 115, -80);
		Scene2D.hideBottom(confirmButton);
		
		stage.setKeyboardFocus(nameField);
		
		stage.addActor(background);
		stage.addActor(optionsBox);
		stage.addActor(nameField);
		stage.addActor(offSoundBox);
		stage.addActor(medSoundBox);
		stage.addActor(highSoundBox);
		stage.addActor(confirmButton);
		
		Gdx.input.setInputProcessor(stage);
		
		/*
		 * Starting animations.
		 */
		Timeline.createParallel().
		push(Tween.to(background, ActorAccessor.ALPHA, 1f).target(1f)).
		push(Tween.to(optionsBox, ActorAccessor.ALPHA, 1f).target(1f)).
		push(Tween.to(nameField, ActorAccessor.POSITION_Y, 1f).target(Scene2D.getCenterY(nameField) + 30)).
		push(Tween.to(confirmButton, ActorAccessor.POSITION_Y, 1f).target(Scene2D.getCenterY(confirmButton) - 130)).
		push(Tween.to(offSoundBox, ActorAccessor.ALPHA, 1f).target(1f)).
		push(Tween.to(medSoundBox, ActorAccessor.ALPHA, 1f).target(1f)).
		push(Tween.to(highSoundBox, ActorAccessor.ALPHA, 1f).target(1f)).
		start(Application.getTweenManager());
		
		/*
		 * Confirm button click listener.
		 */
		confirmButton.addListener(new ClickListener() {

			@Override
			public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
				if (nameField.getText().length() == 0) {
					Settings.setName("Player");
				} else {
					Settings.setName(nameField.getText());
				}
				
				if (offSoundBox.isChecked()) {
					Settings.setVolume("off");
				} else if (medSoundBox.isChecked()) {
					Settings.setVolume("med");
				} else if (highSoundBox.isChecked()) {
					Settings.setVolume("high");
				}
				
				/*
				 * Exiting animations.
				 */
				Timeline.createParallel().beginParallel().
				push(Tween.to(background, ActorAccessor.ALPHA, 1f).target(0f)).
				push(Tween.to(optionsBox, ActorAccessor.ALPHA, 1f).target(0f)).
				push(Tween.to(nameField, ActorAccessor.POSITION_Y, 1f).target(Scene2D.getHideBottomY(nameField))).
				push(Tween.to(confirmButton, ActorAccessor.POSITION_Y, 1f).target(Scene2D.getHideBottomY(confirmButton))).
				push(Tween.to(offSoundBox, ActorAccessor.ALPHA, 1f).target(0f)).
				push(Tween.to(medSoundBox, ActorAccessor.ALPHA, 1f).target(0f)).
				push(Tween.to(highSoundBox, ActorAccessor.ALPHA, 1f).target(0f)).
				setCallback(new TweenCallback() {

					@Override
					public void onEvent(int eventType, BaseTween<?> source) {
						/*
						 * Screen change after animations.
						 */
						Application.setScreen(new MainMenuScreen());
					}
					
				}).
				start(Application.getTweenManager());
			}
			
		});
		
		offSoundBox.addListener(new ClickListener() {
			
			@Override
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
				medSoundBox.setChecked(false);
				highSoundBox.setChecked(false);
				return true;
			}
			
		});
		
		medSoundBox.addListener(new ClickListener() {
			
			@Override
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
				offSoundBox.setChecked(false);
				highSoundBox.setChecked(false);
				return true;
			}
			
		});
		
		highSoundBox.addListener(new ClickListener() {
			
			@Override
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
				offSoundBox.setChecked(false);
				medSoundBox.setChecked(false);
				return true;
			}
			
		});
	}

	@Override
	public void handleInput() {
		if (Gdx.input.isKeyJustPressed(Keys.ESCAPE)) {
			Application.setScreen(new MainMenuScreen());
		}
	}
	
	@Override
	public void update(float deltaTime) {
		stage.act(deltaTime);
		
		Application.getTweenManager().update(deltaTime);
	}
	
	@Override
	public void render() {
		stage.draw();
	}

	@Override
	public void dispose() {
		stage.dispose();
		
		Application.getTweenManager().killAll();
	}

}
