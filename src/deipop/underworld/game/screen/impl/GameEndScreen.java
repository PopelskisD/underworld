package deipop.underworld.game.screen.impl;

import aurelienribon.tweenengine.Timeline;
import aurelienribon.tweenengine.Tween;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;

import deipop.underworld.game.Application;
import deipop.underworld.game.hiscore.Hiscores;
import deipop.underworld.game.level.Level;
import deipop.underworld.game.res.Skins;
import deipop.underworld.game.screen.Screen;
import deipop.underworld.game.util.ActorAccessor;
import deipop.underworld.game.util.Scene2D;

/**
 * @author Deividas Popelskis <deividas.popelskis@gmail.com>
 */
public class GameEndScreen extends Screen {

	/**
	 * The level.
	 */
	private final Level level;
	
	/**
	 * The completed flag.
	 */
	private final boolean completed;
	
	/**
	 * The stage;
	 */
	private Stage stage;
	
	/**
	 * Creates a new game end screen.
	 * @param level - the level.
	 */
	public GameEndScreen(Level level, boolean completed) {
		this.level = level;
		this.completed = completed;
	}
	
	@Override
	public void create() {
		Hiscores.save(level.getPlayer());
		
		stage = new Stage();
		
		/*
		 * Background.
		 */
		Image background = new Image(Skins.getGlobal(), "background");
		background.getColor().a = 0f;
		
		/*
		 * Box.
		 */
		Image box = new Image(Skins.getGlobal(), "box");
		box.getColor().a = 0f;
		
		/*
		 * Title label.
		 */
		Label titleLabel = new Label(completed ? "Game completed" : "You died", Skins.getGlobal());
		titleLabel.getColor().a = 0f;
		
		/*
		 * Text label 1.
		 */
		Label textLabel1 = new Label("You have scored", Skins.getGlobal());
		textLabel1.getColor().a = 0f;
		
		/*
		 * Text label 2.
		 */
		Label textLabel2 = new Label(level.getPlayer().getPoints() + " points!", Skins.getGlobal());
		textLabel2.getColor().a = 0f;
		
		Scene2D.center(box);
		Scene2D.center(titleLabel, 75);
		Scene2D.center(textLabel1);
		Scene2D.center(textLabel2, -35);
		
		stage.addActor(background);
		stage.addActor(box);
		stage.addActor(titleLabel);
		stage.addActor(textLabel1);
		stage.addActor(textLabel2);
		
		/*
		 * Starting animations.
		 */
		Timeline.createParallel().
		push(Tween.to(background, ActorAccessor.ALPHA, 1f).target(1f)).
		push(Tween.to(box, ActorAccessor.ALPHA, 1f).target(1f)).
		push(Tween.to(titleLabel, ActorAccessor.ALPHA, 1f).target(1f)).
		push(Tween.to(textLabel1, ActorAccessor.ALPHA, 1f).target(1f)).
		push(Tween.to(textLabel2, ActorAccessor.ALPHA, 1f).target(1f)).
		start(Application.getTweenManager());
	}
	
	@Override
	public void handleInput() {
		if (Gdx.input.isKeyJustPressed(Keys.ESCAPE)) {
			Application.setScreen(new MainMenuScreen());
		}
	}
	
	@Override
	public void update(float deltaTime) {
		stage.act(deltaTime);
		
		Application.getTweenManager().update(deltaTime);
	}

	@Override
	public void render() {
		stage.draw();
	}

	@Override
	public void dispose() {
		stage.dispose();
		
		Application.getTweenManager().killAll();
	}

}
