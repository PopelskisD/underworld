package deipop.underworld.game.screen.impl;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;

import deipop.underworld.game.Application;
import deipop.underworld.game.level.HUD;
import deipop.underworld.game.level.Level;
import deipop.underworld.game.screen.Screen;

/**
 * @author Deividas Popelskis <deividas.popelskis@gmail.com>
 */
public class GameScreen extends Screen {
	
	/**
	 * The level.
	 */
	private final Level level;
	
	/**
	 * The heads-up display.
	 */
	private final HUD hud;
	
	/**
	 * Creates a new game screen.
	 * @param lvl - the level.
	 */
	public GameScreen(int lvl) {
		level = new Level(lvl);
		hud = new HUD(level);
	}
	
	/**
	 * Creates a new game screen, with saved points from the previous level.
	 * @param points - the points.
	 * @param lvl - the level number.
	 */
	public GameScreen(int points, int lvl) {
		level = new Level(points, lvl);
		hud = new HUD(level);
	}
	
	@Override
	public void handleInput() {
		if (Gdx.input.isKeyJustPressed(Keys.ESCAPE)) {
			Application.setScreen(new MainMenuScreen());
		}
	}
	
	@Override
	public void update(float delta) {
		level.update(delta);
	}

	@Override
	public void render() {
		level.render();
		hud.render();
	}

	@Override
	public void dispose() {
		level.dispose();
	}

}
