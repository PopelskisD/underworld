package deipop.underworld.game.screen.impl;

import aurelienribon.tweenengine.Timeline;
import aurelienribon.tweenengine.Tween;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;

import deipop.underworld.game.Application;
import deipop.underworld.game.hiscore.Hiscores;
import deipop.underworld.game.hiscore.Score;
import deipop.underworld.game.res.Skins;
import deipop.underworld.game.screen.Screen;
import deipop.underworld.game.util.ActorAccessor;
import deipop.underworld.game.util.Scene2D;

/**
 * @author Deividas Popelskis <deividas.popelskis@gmail.com>
 */
public class HiscoresScreen extends Screen {

	/**
	 * The stage;
	 */
	private Stage stage;
	
	/**
	 * The background image.
	 */
	private Image background;
	
	/**
	 * The score labels.
	 */
	private Label[] scoreLabels;
	
	/**
	 * The hiscore box.
	 */
	private Image hiscoreBox;
	
	@Override
	public void create() {
		Hiscores.load();
		
		stage = new Stage();
		
		/*
		 * Background.
		 */
		background = new Image(Skins.getGlobal(), "background");
		background.getColor().a = 0f;
		
		/*
		 * Hiscore box.
		 */
		hiscoreBox = new Image(Skins.getGlobal(), "hiscore_box");
		hiscoreBox.getColor().a = 0f;
		
		/*
		 * Score labels.
		 */
		scoreLabels = new Label[10];
		
		int count = 0;
		
		for (Score score : Hiscores.getScores()) {
			String text = score.getPlayerName() + "     " + score.getScore();
			
			scoreLabels[count++] = new Label(text, Skins.getGlobal());
		}
		
		stage.addActor(background);
		stage.addActor(hiscoreBox);
		
		for (int i = 0; i < count; i++) {
			Scene2D.center(scoreLabels[i], 80 - i * 37 + i);
			
			stage.addActor(scoreLabels[i]);
		}
		
		/*
		 * Starting positions of actors.
		 */
		Scene2D.center(hiscoreBox, -60);
		
		Gdx.input.setInputProcessor(stage);
		
		/*
		 * Starting animations.
		 */
		Timeline.createParallel().
		push(Tween.to(background, ActorAccessor.ALPHA, 1f).target(1f)).
		push(Tween.to(hiscoreBox, ActorAccessor.ALPHA, 1f).target(1f)).
		start(Application.getTweenManager());
	}
	
	@Override
	public void handleInput() {
		if (Gdx.input.isKeyJustPressed(Keys.ESCAPE)) {
			Application.setScreen(new MainMenuScreen());
		}
	}
	
	@Override
	public void update(float deltaTime) {
		stage.act(deltaTime);
		
		Application.getTweenManager().update(deltaTime);
	}

	@Override
	public void render() {
		stage.draw();
	}

	@Override
	public void dispose() {
		stage.dispose();
		
		Application.getTweenManager().killAll();
	}

}