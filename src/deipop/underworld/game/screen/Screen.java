package deipop.underworld.game.screen;

/**
 * @author Deividas Popelskis <deividas.popelskis@gmail.com>
 */
public abstract class Screen {
	
	/**
	 * Creates the screen.
	 */
	public void create() {
		
	}
	
	/**
	 * Handles the screen input.
	 */
	public void handleInput() {
		
	}
	
	/**
	 * Updates the screen.
	 * @param deltaTime - the delta time.
	 */
	public abstract void update(float deltaTime);
	
	/**
	 * Renders the screen.
	 */
	public abstract void render();
	
	/**
	 * Disposes the screen.
	 */
	public abstract void dispose();
	
}
