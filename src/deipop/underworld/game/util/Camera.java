package deipop.underworld.game.util;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;

import deipop.underworld.game.Application;
import deipop.underworld.game.constant.Box2D;
import deipop.underworld.game.level.Level;
import deipop.underworld.game.level.entity.player.Player;

/**
 * @author Deividas Popelskis <deividas.popelskis@gmail.com>
 */
public class Camera extends OrthographicCamera {
	
	/**
	 * Debug camera flag.
	 */
	private final boolean debug;
	
	/**
	 * The minimum x coordinate.
	 */
	private final float minX;
	
	/**
	 * The maximum x coordinate.
	 */
	private final float maxX;
	
	/**
	 * The minimum y coordinate.
	 */
	private final float minY;
	
	/**
	 * The maximum y coordinate.
	 */
	private final float maxY;
	
	/**
	 * Creates a new camera.
	 */
	public Camera() {
		this(false);
	}
	
	/**
	 * Creates a new camera.
	 * @param debug - the debug camera flag.
	 */
	public Camera(boolean debug) {
		super();
		
		this.debug = debug;
		
		minX = Application.WIDTH / 2 / (debug ? Box2D.PPM : 1);
		maxX = minX + Level.WIDTH / (debug ? Box2D.PPM : 1);
		minY = Application.HEIGHT / 2 / (debug ? Box2D.PPM : 1);
		maxY = minY + Level.HEIGHT / (debug ? Box2D.PPM : 1);
	}
	
	/**
	 * Follows the player.
	 * @param player - the player to follow.
	 */
	public void follow(Player player) {
		Vector2 position = debug ? player.getBody().getPosition() : player.getBody().getPosition().scl(Box2D.PPM);
		setPosition(position.x, position.y);
	}
	
	/**
	 * Sets the camera position.
	 * @param x - the x coordinate.
	 * @param y - the y coordinate.
	 */
	private void setPosition(float x, float y) {
		if (x < minX) {
			x = minX;
		}
		
		if (x + viewportWidth > maxX) {
			x = maxX - minX * 2;
		}
		
		if (y < minY) {
			y = minY;
		}
		
		if (y + viewportHeight > maxY) {
			y = maxY - minY * 2;
		}
		
		if (!debug) {
			position.set(MathUtils.round(x), MathUtils.round(y), 0);
		} else {
			position.set(x, y, 0);
		}
	}
	
}
