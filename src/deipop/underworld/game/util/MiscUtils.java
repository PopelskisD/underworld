package deipop.underworld.game.util;

import com.badlogic.gdx.graphics.g2d.TextureRegion;

/**
 * @author Deividas Popelskis <deividas.popelskis@gmail.com>
 */
public class MiscUtils {

	/**
	 * Flips the textures.
	 * @param textures - the textures to flip.
	 */
	public static void flipTextures(TextureRegion[] textures) {
		for (TextureRegion texture : textures) {
			texture.flip(true, false);
		}
	}
	
	/**
	 * Converts integer to a string.
	 * @param integer - the integer to convert.
	 * @param minLength - the minimum length of the string.
	 * @return the string.
	 */
	public static String toString(int integer, int minLength) {
		String value = String.valueOf(integer);
		
		while (value.length() < minLength) {
			value = "0" + value;
		}
		
		return value;
	}
	
}