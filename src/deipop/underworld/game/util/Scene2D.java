package deipop.underworld.game.util;

import com.badlogic.gdx.scenes.scene2d.Actor;

import deipop.underworld.game.Application;

/**
 * @author Deividas Popelskis <deividas.popelskis@gmail.com>
 */
public class Scene2D {
	
	/**
	 * Centers the actor.
	 * @param actor - the actor to center.
	 */
	public static void center(Actor actor) {
		center(actor, 0);
	}
	
	/**
	 * Centers the actor.
	 * @param actor - the actor to center.
	 * @param xOffset - the x offset.
	 * @param yOffset - the y offset.
	 */
	public static void center(Actor actor, int xOffset, int yOffset) {
		actor.setPosition(Application.WIDTH / 2 - actor.getWidth() / 2 + xOffset, Application.HEIGHT / 2 - actor.getHeight() / 2 + yOffset);
	}
	
	/**
	 * Centers the actor.
	 * @param actor - the actor to center.
	 * @param yOffset - the y offset.
	 */
	public static void center(Actor actor, int yOffset) {
		center(actor, 0, yOffset);
	}
	
	/**
	 * Hides the actor at the bottom
	 * @param actor - the actor to hide.
	 */
	public static void hideBottom(Actor actor) {
		actor.setPosition(Application.WIDTH / 2 - actor.getWidth() / 2, 0 - actor.getHeight());
	}
	
	/**
	 * Gets the center y coordainte of an actor.
	 * @param actor - the actor.
	 * @return the center y coordinate.
	 */
	public static float getCenterY(Actor actor) {
		return Application.HEIGHT / 2 - actor.getHeight() / 2;
	}
	
	/**
	 * Gets the bottom y coordinate to hide the actor.
	 * @param actor the actor.
	 * @return the bottom y coordinate to hide the actor.
	 */
	public static float getHideBottomY(Actor actor) {
		return 0 - actor.getHeight();
	}
	
}