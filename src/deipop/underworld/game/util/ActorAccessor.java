package deipop.underworld.game.util;

import com.badlogic.gdx.scenes.scene2d.Actor;

import aurelienribon.tweenengine.TweenAccessor;

/**
 * @author Deividas Popelskis <deividas.popelskis@gmail.com>
 */
public class ActorAccessor implements TweenAccessor<Actor> {
	
	/**
	 * The position x tween type.
	 */
	public static final int POSITION_X = 0;
	
	/**
	 * The position y tween type.
	 */
	public static final int POSITION_Y = 1;
	
	/**
	 * The alpha tween type.
	 */
	public static final int ALPHA = 2;
	
	@Override
	public int getValues(Actor actor, int tweenType, float[] values) {
		switch (tweenType) {
		case POSITION_X:
			values[0] = actor.getX();
			return 1;
		case POSITION_Y:
			values[0] = actor.getY();
			return 1;
		case ALPHA:
			values[0] = actor.getColor().a;
			return 1;
		}
		return 0;
	}

	@Override
	public void setValues(Actor actor, int tweenType, float[] values) {
		switch (tweenType) {
		case POSITION_X:
			actor.setX(values[0]);
			break;
		case POSITION_Y:
			actor.setY(values[0]);
			break;
		case ALPHA:
			actor.getColor().a = values[0];
			break;
		}
	}

}