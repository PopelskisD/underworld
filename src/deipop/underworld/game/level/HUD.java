package deipop.underworld.game.level;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import deipop.underworld.game.Application;
import deipop.underworld.game.res.Textures;
import deipop.underworld.game.util.MiscUtils;

/**
 * @author Deividas Popelskis <deividas.popelskis@gmail.com>
 */
public class HUD {

	/**
	 * The level.
	 */
	private final Level level;
	
	/**
	 * The camera.
	 */
	private final OrthographicCamera camera;
	
	/**
	 * The sprite batch.
	 */
	private final SpriteBatch spriteBatch = Application.getSpriteBatch();
	
	/**
	 * Creates a new heads-up display.
	 * @param level - the level to create the heads-up display for.
	 */
	public HUD(Level level) {
		this.level = level;
		
		camera = new OrthographicCamera();
		camera.setToOrtho(false, Application.WIDTH, Application.HEIGHT);
	}
	
	/**
	 * Renders the heads-up display.
	 */
	public void render() {
		spriteBatch.setProjectionMatrix(camera.combined);
		
		spriteBatch.begin();
		
		/**
		 * Player lives.
		 */
		spriteBatch.draw(Textures.getHUDTexture("heart"), 15, 543);
		spriteBatch.draw(Textures.getHUDTexture("x"), 65, 550);
		spriteBatch.draw(Textures.getHUDTexture(String.valueOf(level.getPlayer().getLives())), 100, 545);
		
		String collectedCoinAmount = MiscUtils.toString(level.getPlayer().getCoinAmount(), 2);
		String totalCoinAmount = MiscUtils.toString(level.getCoinAmount(), 2);
		
		/**
		 * Collected coins.
		 */
		spriteBatch.draw(Textures.getHUDTexture("coin"), 165, 543);
		spriteBatch.draw(Textures.getHUDTexture(collectedCoinAmount.substring(0, 1)), 210, 545);
		spriteBatch.draw(Textures.getHUDTexture(collectedCoinAmount.substring(1, 2)), 240, 545);
		spriteBatch.draw(Textures.getHUDTexture("slash"), 270, 543);
		spriteBatch.draw(Textures.getHUDTexture(totalCoinAmount.substring(0, 1)), 300, 545);
		spriteBatch.draw(Textures.getHUDTexture(totalCoinAmount.substring(1, 2)), 330, 545);
		
		String totalKeyAmount = MiscUtils.toString(level.getKeyAmount(), 2);
		String collectedKeyAmount = MiscUtils.toString(level.getPlayer().getKeyAmount(), 2);
		
		/**
		 * Collected keys.
		 */
		spriteBatch.draw(Textures.getHUDTexture("key"), 390, 543);
		spriteBatch.draw(Textures.getHUDTexture(collectedKeyAmount.substring(0, 1)), 440, 545);
		spriteBatch.draw(Textures.getHUDTexture(collectedKeyAmount.substring(1, 2)), 470, 545);
		spriteBatch.draw(Textures.getHUDTexture("slash"), 500, 543);
		spriteBatch.draw(Textures.getHUDTexture(totalKeyAmount.substring(0, 1)), 530, 545);
		spriteBatch.draw(Textures.getHUDTexture(totalKeyAmount.substring(1, 2)), 560, 545);
		
		int startX = 690;
		String points = MiscUtils.toString(level.getPlayer().getPoints(), 6);
		
		/**
		 * Points.
		 */
		for (char c : points.toCharArray()) {
			spriteBatch.draw(Textures.getHUDTexture("" + c), startX, 543);
			startX += 30;
		}
		
		spriteBatch.end();
	}
	
}
