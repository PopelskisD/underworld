package deipop.underworld.game.level.entity.object.impl;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.physics.box2d.Body;

import deipop.underworld.game.level.entity.object.Type;
import deipop.underworld.game.res.Textures;

/**
 * @author Deividas Popelskis <deividas.popelskis@gmail.com>
 */
public class RedLock extends Lock {

	/**
	 * Creates a new red lock.
	 * @param body - the red lock body.
	 */
	public RedLock(Body body) {
		super(body);
	}
	
	@Override
	public TextureRegion getTexture() {
		return Textures.getObjectTexture("lockRed");
	}
	
	@Override
	public Type getType() {
		return Type.RED_LOCK;
	}

	@Override
	public Type getRequiredKeyType() {
		return Type.RED_KEY;
	}

}
