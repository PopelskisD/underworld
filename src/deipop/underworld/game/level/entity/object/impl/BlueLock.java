package deipop.underworld.game.level.entity.object.impl;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.physics.box2d.Body;

import deipop.underworld.game.level.entity.object.Type;
import deipop.underworld.game.res.Textures;

/**
 * @author Deividas Popelskis <deividas.popelskis@gmail.com>
 */
public class BlueLock extends Lock {

	/**
	 * Creates a new blue lock.
	 * @param body - the blue lock body.
	 */
	public BlueLock(Body body) {
		super(body);
	}
	
	@Override
	public TextureRegion getTexture() {
		return Textures.getObjectTexture("lockBlue");
	}

	@Override
	public Type getType() {
		return Type.BLUE_LOCK;
	}

	@Override
	public Type getRequiredKeyType() {
		return Type.BLUE_KEY;
	}
	
}
