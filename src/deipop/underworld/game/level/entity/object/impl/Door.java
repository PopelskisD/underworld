package deipop.underworld.game.level.entity.object.impl;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.Timer.Task;

import deipop.underworld.game.level.Level;
import deipop.underworld.game.level.entity.Entity;
import deipop.underworld.game.level.entity.object.Object;
import deipop.underworld.game.level.entity.object.Type;
import deipop.underworld.game.res.Sounds;
import deipop.underworld.game.res.Textures;

/**
 * @author Deividas Popelskis <deividas.popelskis@gmail.com>
 */
public class Door extends Object {
	
	/**
	 * The opened flag.
	 */
	private boolean opened;
	
	/**
	 * Creates a new dor.
	 * @param body - the door body.
	 */
	public Door(Body body) {
		super(body);
	}

	@Override
	public TextureRegion getTexture() {
		return Textures.getObjectTexture(opened ? "doorOpened" : "doorClosed");
	}

	@Override
	public Type getType() {
		return Type.DOOR;
	}
	
	@Override
	public void onCollisionStart(Level level, Entity collidingEntity) {
		if (opened) {
			Timer.schedule(new Task() {

				@Override
				public void run() {
					level.advance();
				}
				
			}, 0.5f);
		}
	}
	
	/**
	 * Opens the door.
	 */
	public void open() {
		opened = true;
		
		Sounds.play("open_door");
	}
	
	/**
	 * Checks if the door is opened.
	 * @return true - if the door is opened, false - otherwise.
	 */
	public boolean isOpen() {
		return opened;
	}
	
}