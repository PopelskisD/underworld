package deipop.underworld.game.level.entity.object.impl;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.physics.box2d.Body;

import deipop.underworld.game.level.entity.Entity;

/**
 * @author Deividas Popelskis <deividas.popelskis@gmail.com>
 */
public class GroundTile extends Entity {

	/**
	 * Creates a new ground tile.
	 * @param body - the ground tile body.
	 */
	public GroundTile(Body body) {
		super(body);
	}

	@Override
	public void render(SpriteBatch spriteBatch) {}

}
