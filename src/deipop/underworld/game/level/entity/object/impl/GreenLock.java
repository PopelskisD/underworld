package deipop.underworld.game.level.entity.object.impl;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.physics.box2d.Body;

import deipop.underworld.game.level.entity.object.Type;
import deipop.underworld.game.res.Textures;

/**
 * @author Deividas Popelskis <deividas.popelskis@gmail.com>
 */
public class GreenLock extends Lock {
	
	/**
	 * Creates a new green lock.
	 * @param body - the green lock body.
	 */
	public GreenLock(Body body) {
		super(body);
	}
	
	@Override
	public TextureRegion getTexture() {
		return Textures.getObjectTexture("lockGreen");
	}

	@Override
	public Type getType() {
		return Type.GREEN_LOCK;
	}

	@Override
	public Type getRequiredKeyType() {
		return Type.GREEN_KEY;
	}
	
}