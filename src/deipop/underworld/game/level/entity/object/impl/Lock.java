package deipop.underworld.game.level.entity.object.impl;

import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.Timer.Task;

import deipop.underworld.game.level.Level;
import deipop.underworld.game.level.entity.Entity;
import deipop.underworld.game.level.entity.object.Object;
import deipop.underworld.game.level.entity.object.Type;
import deipop.underworld.game.res.Sounds;

/**
 * @author Deividas Popelskis <deividas.popelskis@gmail.com>
 */
public abstract class Lock extends Object {

	/**
	 * Creates a new lock
	 * @param body - the lock body.
	 */
	public Lock(Body body) {
		super(body);
	}
	
	@Override
	public void onCollisionStart(Level level, Entity collidingEntity) {
		if (level.getObjectAmount(getRequiredKeyType()) == 0) {
			destroy();

			Sounds.play("open_lock");
			
			Timer.schedule(new Task() {

				@Override
				public void run() {
					level.updateDoor();
				}
				
			}, 0.5f);
		}
	}
	
	/**
	 * Gets the required key type to open this lock.
	 * @return the required key type.
	 */
	public abstract Type getRequiredKeyType();

}
