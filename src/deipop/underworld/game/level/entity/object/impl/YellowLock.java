package deipop.underworld.game.level.entity.object.impl;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.physics.box2d.Body;

import deipop.underworld.game.level.entity.object.Type;
import deipop.underworld.game.res.Textures;

/**
 * @author Deividas Popelskis <deividas.popelskis@gmail.com>
 */
public class YellowLock extends Lock {

	/**
	 * Creates a new yellow lock.
	 * @param body - the yellow lock body.
	 */
	public YellowLock(Body body) {
		super(body);
	}
	
	@Override
	public TextureRegion getTexture() {
		return Textures.getObjectTexture("lockYellow");
	}

	@Override
	public Type getType() {
		return Type.YELLOW_LOCK;
	}

	@Override
	public Type getRequiredKeyType() {
		return Type.YELLOW_KEY;
	}

}