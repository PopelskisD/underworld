package deipop.underworld.game.level.entity.object.impl;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.physics.box2d.Body;

import deipop.underworld.game.level.Level;
import deipop.underworld.game.level.entity.Entity;
import deipop.underworld.game.level.entity.player.PlayerFoot;

/**
 * @author Deividas Popelskis <deividas.popelskis@gmail.com>
 */
public class WaterTile extends Entity {

	/**
	 * Creates a new water tile.
	 * @param body - the water tile body.
	 */
	public WaterTile(Body body) {
		super(body);
	}

	@Override
	public void render(SpriteBatch spriteBatch) {}
	
	@Override
	public void onCollisionStart(Level level, Entity collidingEntity) {
		if (collidingEntity instanceof PlayerFoot) {
			level.end();
		}
	}

}
