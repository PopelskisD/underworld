package deipop.underworld.game.level.entity.object.item.impl;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.physics.box2d.Body;

import deipop.underworld.game.level.entity.object.Type;
import deipop.underworld.game.res.Textures;

/**
 * @author Deividas Popelskis <deividas.popelskis@gmail.com>
 */
public class BronzeCoin extends Coin {
	
	/**
	 * Creates a new bronze coin.
	 * @param body - the bronze coin body.
	 */
	public BronzeCoin(Body body) {
		super(body);
	}
	
	@Override
	public TextureRegion getTexture() {
		return Textures.getItemTexture("coinBronze");
	}
	
	@Override
	public int getPoints() {
		return 20;
	}

	@Override
	public Type getType() {
		return Type.BRONZE_COIN;
	}
	
}
