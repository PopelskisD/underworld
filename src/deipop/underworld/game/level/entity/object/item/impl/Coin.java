package deipop.underworld.game.level.entity.object.item.impl;

import com.badlogic.gdx.physics.box2d.Body;

import deipop.underworld.game.level.entity.object.item.Item;

/**
 * @author Deividas Popelskis <deividas.popelskis@gmail.com>
 */
public abstract class Coin extends Item {

	/**
	 * Creates a new coin.
	 * @param body - the coin body.
	 */
	public Coin(Body body) {
		super(body);
	}

}
