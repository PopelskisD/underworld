package deipop.underworld.game.level.entity.object.item.impl;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.physics.box2d.Body;

import deipop.underworld.game.level.entity.object.Type;
import deipop.underworld.game.res.Textures;

/**
 * @author Deividas Popelskis <deividas.popelskis@gmail.com>
 */
public class SilverCoin extends Coin {
	
	/**
	 * Creates a new silver coin.
	 * @param body - the silver coin body.
	 */
	public SilverCoin(Body body) {
		super(body);
	}
	
	@Override
	public TextureRegion getTexture() {
		return Textures.getItemTexture("coinSilver");
	}
	
	@Override
	public int getPoints() {
		return 40;
	}

	@Override
	public Type getType() {
		return Type.SILVER_COIN;
	}
	
}
