package deipop.underworld.game.level.entity.object.item.impl;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.physics.box2d.Body;

import deipop.underworld.game.level.entity.object.Type;
import deipop.underworld.game.res.Textures;

/**
 * @author Deividas Popelskis <deividas.popelskis@gmail.com>
 */
public class RedKey extends Key {

	/**
	 * Creates a new red key.
	 * @param body - the red key body.
	 */
	public RedKey(Body body) {
		super(body);
	}
	
	@Override
	public TextureRegion getTexture() {
		return Textures.getItemTexture("keyRed");
	}
	
	@Override
	public int getPoints() {
		return 45;
	}

	@Override
	public Type getType() {
		return Type.RED_KEY;
	}

}
