package deipop.underworld.game.level.entity.object.item.impl;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.physics.box2d.Body;

import deipop.underworld.game.level.entity.object.Type;
import deipop.underworld.game.res.Textures;

/**
 * @author Deividas Popelskis <deividas.popelskis@gmail.com>
 */
public class BlueKey extends Key {

	/**
	 * Creates a new blue key.
	 * @param body - the blue key body.
	 */
	public BlueKey(Body body) {
		super(body);
	}
	
	@Override
	public TextureRegion getTexture() {
		return Textures.getItemTexture("keyBlue");
	}
	
	@Override
	public int getPoints() {
		return 25;
	}

	@Override
	public Type getType() {
		return Type.BLUE_KEY;
	}
	
}
