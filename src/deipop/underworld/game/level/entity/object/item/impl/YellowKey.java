package deipop.underworld.game.level.entity.object.item.impl;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.physics.box2d.Body;

import deipop.underworld.game.level.entity.object.Type;
import deipop.underworld.game.res.Textures;

/**
 * @author Deividas Popelskis <deividas.popelskis@gmail.com>
 */
public class YellowKey extends Key {

	/**
	 * Creates a new yellow key.
	 * @param body - the yellow key body.
	 */
	public YellowKey(Body body) {
		super(body);
	}
	
	@Override
	public TextureRegion getTexture() {
		return Textures.getItemTexture("keyYellow");
	}
	
	@Override
	public int getPoints() {
		return 55;
	}

	@Override
	public Type getType() {
		return Type.YELLOW_KEY;
	}

}