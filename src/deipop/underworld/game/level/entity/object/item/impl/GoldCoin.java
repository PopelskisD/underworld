package deipop.underworld.game.level.entity.object.item.impl;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.physics.box2d.Body;

import deipop.underworld.game.level.entity.object.Type;
import deipop.underworld.game.res.Textures;

/**
 * @author Deividas Popelskis <deividas.popelskis@gmail.com>
 */
public class GoldCoin extends Coin {
	
	/**
	 * Creates a new gold coin.
	 * @param body - the bronze coin body.
	 */
	public GoldCoin(Body body) {
		super(body);
	}
	
	@Override
	public TextureRegion getTexture() {
		return Textures.getItemTexture("coinGold");
	}
	
	@Override
	public int getPoints() {
		return 60;
	}

	@Override
	public Type getType() {
		return Type.GOLD_COIN;
	}
	
}
