package deipop.underworld.game.level.entity.object.item.impl;

import com.badlogic.gdx.physics.box2d.Body;

import deipop.underworld.game.level.entity.object.item.Item;

/**
 * @author Deividas Popelskis <deividas.popelskis@gmail.com>
 */
public abstract class Key extends Item {

	/**
	 * Creates a new key.
	 * @param body - the key body.
	 */
	public Key(Body body) {
		super(body);
	}

}
