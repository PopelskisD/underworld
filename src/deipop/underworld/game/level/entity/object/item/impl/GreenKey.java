package deipop.underworld.game.level.entity.object.item.impl;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.physics.box2d.Body;

import deipop.underworld.game.level.entity.object.Type;
import deipop.underworld.game.res.Textures;

/**
 * @author Deividas Popelskis <deividas.popelskis@gmail.com>
 */
public class GreenKey extends Key {
	
	/**
	 * Creates a new green key.
	 * @param body - the green key body.
	 */
	public GreenKey(Body body) {
		super(body);
	}
	
	@Override
	public TextureRegion getTexture() {
		return Textures.getItemTexture("keyGreen");
	}
	
	@Override
	public int getPoints() {
		return 35;
	}

	@Override
	public Type getType() {
		return Type.GREEN_KEY;
	}
	
}