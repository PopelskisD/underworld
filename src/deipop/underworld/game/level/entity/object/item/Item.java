package deipop.underworld.game.level.entity.object.item;

import com.badlogic.gdx.physics.box2d.Body;

import deipop.underworld.game.level.Level;
import deipop.underworld.game.level.entity.Entity;
import deipop.underworld.game.level.entity.object.Object;
import deipop.underworld.game.level.entity.object.item.impl.Coin;
import deipop.underworld.game.level.entity.object.item.impl.Key;
import deipop.underworld.game.level.entity.player.Player;
import deipop.underworld.game.res.Sounds;

/**
 * @author Deividas Popelskis <deividas.popelskis@gmail.com>
 */
public abstract class Item extends Object {

	/**
	 * Creates a new item.
	 * @param body - the item body.
	 */
	public Item(Body body) {
		super(body);
	}
	
	@Override
	public void onCollisionStart(Level level, Entity collidingEntity) {
		if (!(collidingEntity instanceof Player)) {
			return;
		}
		
		Player player = (Player) collidingEntity;
		player.collect(this);
		
		if (this instanceof Coin) {
			Sounds.play("collect_coin");
		} else if (this instanceof Key) {
			Sounds.play("collect_key");
		}
		
		destroy();
	}

	/**
	 * Gets the points.
	 * @return the points this item gives.
	 */
	public abstract int getPoints();
	
}
