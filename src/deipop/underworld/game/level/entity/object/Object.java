package deipop.underworld.game.level.entity.object;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.physics.box2d.Body;

import deipop.underworld.game.constant.Box2D;
import deipop.underworld.game.level.entity.Entity;

/**
 * @author Deividas Popelskis <deividas.popelskis@gmail.com>
 */
public abstract class Object extends Entity {

	/**
	 * Creates a new object.
	 * @param body - the object body.
	 */
	public Object(Body body) {
		super(body);
	}
	
	@Override
	public void render(SpriteBatch spriteBatch) {
		spriteBatch.draw(getTexture(), MathUtils.round(body.getPosition().x * Box2D.PPM - getTexture().getRegionWidth() / 2), MathUtils.round(body.getPosition().y * Box2D.PPM - getTexture().getRegionHeight() / 2));
	}

	/**
	 * Gets this object texture.
	 * @return the object texture.
	 */
	public abstract TextureRegion getTexture();
	
	/**
	 * Gets the object type.
	 * @return the object type.
	 */
	public abstract Type getType();

}
