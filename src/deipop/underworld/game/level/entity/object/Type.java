package deipop.underworld.game.level.entity.object;

/**
 * @author Deividas Popelskis <deividas.popelskis@gmail.com>
 */
public enum Type {
	BRONZE_COIN,
	SILVER_COIN,
	GOLD_COIN,
	
	YELLOW_KEY,
	GREEN_KEY,
	BLUE_KEY,
	RED_KEY,
	
	YELLOW_LOCK,
	GREEN_LOCK,
	BLUE_LOCK,
	RED_LOCK,
	
	DOOR;
}