package deipop.underworld.game.level.entity;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.physics.box2d.Body;

import deipop.underworld.game.level.Level;

/**
 * @author Deividas Popelskis <deividas.popelskis@gmail.com>
 */
public abstract class Entity {
	
	/**
	 * The body.
	 */
	protected final Body body;
	
	/**
	 * The destroyed flag.
	 */
	private boolean destroyed;
	
	/**
	 * Creates a new entity.
	 * @param body - the entity body.
	 */
	public Entity(Body body) {
		this.body = body;
	}
	
	/**
	 * Renders the entity.
	 * @param spriteBatch - the sprite batch.
	 */
	public abstract void render(SpriteBatch spriteBatch);
	
	/**
	 * Collision start event.
	 * @param level - the level.
	 * @param collidingEntity - the colliding entity.
	 */
	public void onCollisionStart(Level level, Entity collidingEntity) {}
	
	/**
	 * Collision end event.
	 * @param level - the level.
	 * @param collidingEntity - the colliding entity.
	 */
	public void onCollisionEnd(Level level, Entity collidingEntity) {}
	
	/**
	 * Gets the body.
	 * @return the body.
	 */
	public Body getBody() {
		return body;
	}
	
	/**
	 * Destroys the entity.
	 */
	public void destroy() {
		destroyed = true;
	}
	
	/**
	 * Checks if the entity is destroyed.
	 * @return true - if the entity is destroyed, false - otherwise.
	 */
	public boolean isDestroyed() {
		return destroyed;
	}
	
}