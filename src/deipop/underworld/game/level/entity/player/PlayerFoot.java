package deipop.underworld.game.level.entity.player;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.physics.box2d.Body;

import deipop.underworld.game.level.Level;
import deipop.underworld.game.level.entity.Entity;
import deipop.underworld.game.level.entity.object.impl.WaterTile;

/**
 * @author Deividas Popelskis <deividas.popelskis@gmail.com>
 */
public class PlayerFoot extends Entity {

	/**
	 * Creates a new player foot.
	 * @param body - the player foot body.
	 */
	public PlayerFoot(Body body) {
		super(body);
	}

	@Override
	public void onCollisionStart(Level level, Entity collidingEntity) {
		if (collidingEntity instanceof WaterTile) {
			return;
		}
		
		level.getPlayer().incrementFootSteps();
		
	}
	
	@Override
	public void onCollisionEnd(Level level, Entity collidingEntity) {
		if (collidingEntity instanceof WaterTile) {
			return;
		}
		
		level.getPlayer().decrementFootSteps();
	}
	
	@Override
	public void render(SpriteBatch spriteBatch) { }

}
