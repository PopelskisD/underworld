package deipop.underworld.game.level.entity.player;

/**
 * @author Deividas Popelskis <deividas.popelskis@gmail.com>
 */
public enum State {
	STANDING,
	WALKING
}
