package deipop.underworld.game.level.entity.player;

import java.util.HashMap;
import java.util.Map;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Filter;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.Timer.Task;

import deipop.underworld.game.constant.Box2D;
import deipop.underworld.game.level.Level;
import deipop.underworld.game.level.entity.CharacterEntity;
import deipop.underworld.game.level.entity.object.Type;
import deipop.underworld.game.level.entity.object.item.Item;
import deipop.underworld.game.res.Sounds;
import deipop.underworld.game.res.Textures;
import deipop.underworld.game.util.MiscUtils;

/**
 * @author Deividas Popelskis <deividas.popelskis@gmail.com>
 */
public class Player extends CharacterEntity {
	
	/**
	 * The maximum amount of lives a player can have.
	 */
	public static final int MAX_LIVES = 3;
	
	/**
	 * The starting x coordinate of the player.
	 */
	public static final float STARTING_POSITION_X = 100f;
	
	/**
	 * The starting y coordinate of the player.
	 */
	public static final float STARTING_POSITION_Y = 300f;
	
	/**
	 * The width of the player.
	 */
	public static final float WIDTH = 60f;
	
	/**
	 * The height of the player.
	 */
	public static final float HEIGHT = 81f;
	
	/**
	 * The player mass.
	 */
	public static final float MASS = 100f;
	
	/**
	 * The player walking velocity.
	 */
	public static final float WALKING_VELOCITY = 3.5f;
	
	/**
	 * The player jumping velocity.
	 */
	public static final float JUMPING_VELOCITY = 6.5f;
	
	/**
	 * The player stand texture.
	 */
	private final TextureRegion standTexture;
	
	/**
	 * The player walk animation.
	 */
	private final Animation walkAnimation;
	
	/**
	 * The amount of points the player has.
	 */
	private int points = 0;
	
	/**
	 * The collected item map.
	 */
	private final Map<Type, Integer> collectedItems = new HashMap<Type, Integer>();
	
	/**
	 * The current player state.
	 */
	private State state = State.STANDING;
	
	/**
	 * The foot steps.
	 */
	private int footSteps;
	
	/**
	 * Creates a new player.
	 * @param body - the player body.
	 */
	public Player(Body body) {
		super(body, MAX_LIVES);
		
		standTexture = Textures.getPlayerTexture("stand");
		walkAnimation = new Animation(0.07f, Textures.getPlayerWalkTextures());
	}
	
	/**
	 * Used to determine the current walking animation frame.
	 */
	private float time = 0f;
	
	/**
	 * Time, when the last time the sound was played.
	 */
	private long lastCall;
	
	@Override
	public void update(float deltaTime) {
		if (isDestroyed()) {
			return;
		}
		
		time += deltaTime;
		
		float velocityX = 0f;
		float velocityY = 0f;
		
		state = State.STANDING;
		
		if (Gdx.input.isKeyPressed(Keys.RIGHT) || Gdx.input.isKeyPressed(Keys.LEFT) || Gdx.input.isKeyPressed(Keys.UP)) {
			/*
			 * Walking to the right.
			 */
			if (Gdx.input.isKeyPressed(Keys.RIGHT)) {
				velocityX = WALKING_VELOCITY;
				
				if (standTexture.isFlipX()) {
					standTexture.flip(true, false);
				}
				
				if (walkAnimation.getKeyFrames()[0].isFlipX()) {
					MiscUtils.flipTextures(walkAnimation.getKeyFrames());
				}
				
				float velocityXChange = velocityX - body.getLinearVelocity().x;
				float impulseX = body.getMass() * velocityXChange;
				
				body.applyLinearImpulse(new Vector2(impulseX, 0), body.getWorldCenter(), true);
				
				if (body.getPosition().x >= 61.05f) {
					body.setLinearVelocity(0f, body.getLinearVelocity().y);
				}
				
				state = State.WALKING;
			}
			
			/*
			 * Walking to the left.
			 */
			if (Gdx.input.isKeyPressed(Keys.LEFT)) {
				velocityX = -WALKING_VELOCITY;
				
				if (!standTexture.isFlipX()) {
					standTexture.flip(true, false);
				}
				
				if (!walkAnimation.getKeyFrames()[0].isFlipX()) {
					MiscUtils.flipTextures(walkAnimation.getKeyFrames());
				}
				
				float velocityXChange = velocityX - body.getLinearVelocity().x;
				float impulseX = body.getMass() * velocityXChange;
				
				body.applyLinearImpulse(new Vector2(impulseX, 0), body.getWorldCenter(), true);
			
				if (body.getPosition().x <= 0.40f) {
					body.setLinearVelocity(0f, body.getLinearVelocity().y);
				}
				
				state = State.WALKING;
			}
			
			/*
			 * Jumping.
			 */
			if (Gdx.input.isKeyPressed(Keys.UP)) {
				if (footSteps > 0) {
					if (System.currentTimeMillis() - lastCall >= 100) {
						Sounds.play("jump");
						
						lastCall = System.currentTimeMillis();
					}
					
					velocityY = JUMPING_VELOCITY;
					
					float velocityYChange = velocityY - body.getLinearVelocity().y;
					float impulseY = body.getMass() * velocityYChange;
					
					body.applyLinearImpulse(new Vector2(0, impulseY), body.getWorldCenter(), true);
				}
			}
		} else {
			body.setLinearVelocity(0, body.getLinearVelocity().y);
		}
	}

	@Override
	public void render(SpriteBatch spriteBatch) {
		float drawX = MathUtils.round(body.getPosition().x * Box2D.PPM - (standTexture.getRegionWidth() / 2));
		float drawY = MathUtils.round(body.getPosition().y * Box2D.PPM - (standTexture.getRegionHeight() / 2));
		
		switch (state) {
		case STANDING:
			spriteBatch.draw(standTexture, drawX, drawY);
			break;
		case WALKING:
			spriteBatch.draw(walkAnimation.getKeyFrame(time, true), drawX, drawY);
			break;
		}
	}
	
	public void hit(int damage, Level level) {
		lives -= damage;
		
		if (lives < 0) {
			lives = 0;
		}
		
		if (getLives() == 0) {
			Filter filterData = getBodyFixture().getFilterData();
			filterData.maskBits = 0;
			getBodyFixture().setFilterData(filterData);
			
			destroy();
			
			Timer.schedule(new Task() {

				@Override
				public void run() {
					level.end();
				}
				
			}, 0.5f);
		} else {
			jump();
		}
	}
	
	/**
	 * Makes the player jump.
	 */
	public void jump() {
		float velocityYChange = JUMPING_VELOCITY - body.getLinearVelocity().y;
		float impulseY = body.getMass() * velocityYChange;
		
		body.applyLinearImpulse(new Vector2(0, impulseY), body.getWorldCenter(), true);
	}
	
	/**
	 * Collects an item.
	 * @param item - the item to collect.
	 */
	public void collect(Item item) {
		points += item.getPoints();
		
		Integer amount = collectedItems.get(item.getType());
		
		if (amount == null) {
			collectedItems.put(item.getType(), 1);
		} else {
			collectedItems.put(item.getType(), amount + 1);
		}
		
		item.destroy();
	}
	
	/**
	 * Adds points.
	 * @param amount - the amount to add.
	 */
	public void addPoints(int amount) {
		points += amount;
	}
	
	/**
	 * Gets the point amount.
	 * @return the point amount.
	 */
	public int getPoints() {
		return points;
	}
	
	/**
	 * Gets the amount of the specified item type.
	 * @param itemType - the item type.
	 * @return the item amount.
	 */
	public int getItemAmount(Type itemType) {
		return collectedItems.getOrDefault(itemType, 0);
	}
	
	/**
	 * Gets the amount of coins the player has collected.
	 * @return the coin amount.
	 */
	public int getCoinAmount() {
		int amount = 0;
		
		amount += getItemAmount(Type.BRONZE_COIN);
		amount += getItemAmount(Type.SILVER_COIN);
		amount += getItemAmount(Type.GOLD_COIN);
		
		return amount;
	}
	
	/**
	 * Gets the amount of keys the player has collected.
	 * @return the key amount.
	 */
	public int getKeyAmount() {
		int amount = 0;
		
		amount += getItemAmount(Type.YELLOW_KEY);
		amount += getItemAmount(Type.GREEN_KEY);
		amount += getItemAmount(Type.BLUE_KEY);
		amount += getItemAmount(Type.RED_KEY);
		
		return amount;
	}
	
	/**
	 * Gets the body fixture.
	 * @return the body fixture.
	 */
	public Fixture getBodyFixture() {
		return body.getFixtureList().get(0);
	}
	
	/**
	 * Increments the foot steps.
	 */
	public void incrementFootSteps() {
		footSteps++;
	}
	
	/**
	 * Decrements the foot steps.
	 */
	public void decrementFootSteps() {
		footSteps--;
	}
	
}