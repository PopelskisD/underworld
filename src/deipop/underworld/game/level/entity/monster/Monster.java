package deipop.underworld.game.level.entity.monster;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;

import deipop.underworld.game.constant.Box2D;
import deipop.underworld.game.level.Level;
import deipop.underworld.game.level.entity.CharacterEntity;
import deipop.underworld.game.level.entity.Entity;
import deipop.underworld.game.level.entity.player.Player;
import deipop.underworld.game.level.entity.player.PlayerFoot;
import deipop.underworld.game.res.Sounds;

/**
 * @author Deividas Popelskis <deividas.popelskis@gmail.com>
 */
public abstract class Monster extends CharacterEntity {
	
	/**
	 * Walking right flag.
	 */
	private boolean walkingRight;
	
	/**
	 * Jumping up flag.
	 */
	private boolean jumpingUp;
	
	/**
	 * The starting position of this monster.
	 */
	private Vector2 startingPosition;
	
	/**
	 * Time used for rendering walking animations.
	 */
	private float time;
	
	/**
	 * The level this monster is spawned at.
	 */
	private int level;
	
	/**
	 * Creates a new monster.
	 * @param body - the body of the monster.
	 * @param lives - the amount of lives this monster has.
	 */
	public Monster(int level, Body body, int lives) {
		super(body, lives * level);
		
		this.level = level;
		
		startingPosition = body.getPosition().cpy();
	}
	
	@Override
	public void update(float deltaTime) {
		time += deltaTime;
		
		if (getVelocityY() == 0) {
			if (Math.abs(body.getPosition().x - startingPosition.x) >= getWalkingDistance()) {
				walkingRight = !walkingRight;
			}
			
			if (walkingRight) {
				body.setLinearVelocity(getVelocityX(), body.getLinearVelocity().y);
			} else {
				body.setLinearVelocity(-getVelocityX(), body.getLinearVelocity().y);
			}
		} else {
			if (Math.abs(body.getPosition().y - startingPosition.y) >= getWalkingDistance()) {
				jumpingUp = !jumpingUp;
			}
			
			if (jumpingUp) {
				body.setLinearVelocity(body.getLinearVelocity().x, getVelocityY());
			} else {
				body.setLinearVelocity(body.getLinearVelocity().x, -getVelocityY());
			}
		}
	}

	@Override
	public void render(SpriteBatch spriteBatch) {
		float drawX = MathUtils.round(body.getPosition().x * Box2D.PPM - (getWalkAnimation().getKeyFrames()[0].getRegionWidth() / 2));
		float drawY = MathUtils.round(body.getPosition().y * Box2D.PPM - (getWalkAnimation().getKeyFrames()[0].getRegionHeight() / 2));
		
		TextureRegion currentFrame = getWalkAnimation().getKeyFrame(time, true);
		
		spriteBatch.draw(currentFrame, walkingRight ? currentFrame.getRegionWidth() + drawX : drawX, drawY, walkingRight ? -currentFrame.getRegionWidth() : currentFrame.getRegionWidth(), currentFrame.getRegionHeight());
	}
	
	/**
	 * Gets the walking animation of this monster.
	 * @return the walking animation.
	 */
	public abstract Animation getWalkAnimation();
	
	/**
	 * Gets the x velocity.
	 * @return the x velocity.
	 */
	public abstract float getVelocityX();
	
	/**
	 * Gets the y velocity.
	 * @return the y velocity.
	 */
	public abstract float getVelocityY();
	
	/**
	 * Gets the distance this monster can walk before turning to the other side.
	 * @return the distance this monster can walk.
	 */
	public abstract float getWalkingDistance();
	
	/**
	 * Gets the damage this monster deals.
	 * @return the damage this monster deals.
	 */
	public abstract int getDamage();
	
	/**
	 * Time since the last collision event was called.
	 */
	private long lastCall;
	
	/**
	 * Collision start event.
	 */
	public void onCollisionStart(Level level, Entity collidingEntity) {
		if (System.currentTimeMillis() - lastCall < 1000) {
			return;
		}
		
		if (collidingEntity instanceof PlayerFoot) {
			hit(1);
			
			Player player = (Player) collidingEntity.getBody().getUserData();
			
			if (getLives() == 0) {
				player.addPoints(getPoints() * this.level);
				
				Sounds.play("defeat");
			} else {
				player.jump();
				
				Sounds.play("hit_monster");
			}
		} else if (collidingEntity instanceof Player) {
			Player player = (Player) collidingEntity;
			player.hit(getDamage() * this.level, level);
			
			if (player.getLives() > 0) {
				Sounds.play("hit_player");
			} else {
				Sounds.play("defeat");
			}
		}
		
		lastCall = System.currentTimeMillis();
	}
	
	/**
	 * Gets the point amount this monster gives on death.
	 * @return the point amount.
	 */
	public abstract int getPoints();

}
