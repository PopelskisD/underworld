package deipop.underworld.game.level.entity.monster.impl;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.physics.box2d.Body;

import deipop.underworld.game.level.entity.monster.Monster;
import deipop.underworld.game.res.Textures;

/**
 * @author Deividas Popelskis <deividas.popelskis@gmail.com>
 */
public class Fly extends Monster {

	/**
	 * The life amount.
	 */
	public static final int LIVES = 3;
	
	/**
	 * Creates a new fly.
	 * @param level - the level this fly is created in.
	 * @param body - the fly body.
	 */
	public Fly(int level, Body body) {
		super(level, body, LIVES);
	}

	@Override
	public Animation getWalkAnimation() {
		return new Animation(0.2f, Textures.getMonsterWalkTextures("fly"));
	}

	@Override
	public float getVelocityX() {
		return 2f;
	}
	
	@Override
	public float getVelocityY() {
		return 0f;
	}

	@Override
	public float getWalkingDistance() {
		return 1.5f;
	}

	@Override
	public int getDamage() {
		return 1;
	}

	@Override
	public int getPoints() {
		return 50;
	}

}
