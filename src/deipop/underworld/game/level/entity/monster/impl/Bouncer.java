package deipop.underworld.game.level.entity.monster.impl;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.physics.box2d.Body;

import deipop.underworld.game.level.entity.monster.Monster;
import deipop.underworld.game.res.Textures;

/**
 * @author Deividas Popelskis <deividas.popelskis@gmail.com>
 */
public class Bouncer extends Monster {

	/**
	 * The life amount.
	 */
	public static final int LIVES = 4;
	
	/**
	 * Creates a new bouncer.
	 * @param level - the level this bouncer is created in.
	 * @param body - the bouncer body.
	 */
	public Bouncer(int level, Body body) {
		super(level, body, LIVES);
	}

	@Override
	public Animation getWalkAnimation() {
		return new Animation(2f, Textures.getMonsterWalkTextures("bouncer"));
	}

	@Override
	public float getVelocityX() {
		return 0f;
	}

	@Override
	public float getVelocityY() {
		return 1f;
	}

	@Override
	public float getWalkingDistance() {
		return 1f;
	}

	@Override
	public int getDamage() {
		return 1;
	}

	@Override
	public int getPoints() {
		return 100;
	}

}