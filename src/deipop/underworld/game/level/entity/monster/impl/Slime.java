package deipop.underworld.game.level.entity.monster.impl;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.physics.box2d.Body;

import deipop.underworld.game.level.entity.monster.Monster;
import deipop.underworld.game.res.Textures;

/**
 * @author Deividas Popelskis <deividas.popelskis@gmail.com>
 */
public class Slime extends Monster {
	
	/**
	 * The life amount.
	 */
	public static final int LIVES = 2;
	
	/**
	 * Creates a new slime.
	 * @param level - the level this slime is created in.
	 * @param body - the slime body.
	 */
	public Slime(int level, Body body) {
		super(level, body, LIVES);
	}
	
	@Override
	public Animation getWalkAnimation() {
		return new Animation(0.2f, Textures.getMonsterWalkTextures("slime"));
	}
	
	@Override
	public float getVelocityX() {
		return 1.5f;
	}
	
	@Override
	public float getVelocityY() {
		return 0f;
	}
	
	@Override
	public float getWalkingDistance() {
		return 1f;
	}

	@Override
	public int getDamage() {
		return 1;
	}
	
	@Override
	public int getPoints() {
		return 10;
	}
	
}
