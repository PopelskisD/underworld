package deipop.underworld.game.level.entity;

import com.badlogic.gdx.physics.box2d.Body;

/**
 * @author Deividas Popelskis <deividas.popelskis@gmail.com>
 */
public abstract class CharacterEntity extends Entity {

	/**
	 * The life amount.
	 */
	protected int lives;
	
	/**
	 * Creates a new character entity.
	 * @param body - the body.
	 * @param lives - the life amount.
	 */
	public CharacterEntity(Body body, int lives) {
		super(body);
		
		this.lives = lives;
	}
	
	/**
	 * Updates this entity.
	 */
	public abstract void update(float deltaTime);
	
	/**
	 * Time, this entity was hit.
	 */
	private long lastHit;
	
	/**
	 * Hits this entity.
	 */
	public void hit() {
		hit(1);
	}
	
	/**
	 * Hits this entity.
	 * @param damage - the damage to deal.
	 */
	public void hit(int damage) {
		if (canHit()) {
			lastHit = System.currentTimeMillis();
			lives -= damage;
			
			if (lives < 0) {
				lives = 0;
			}
			
			if (lives == 0) {
				destroy();
			}
		}
	}
	
	/**
	 * Checks if this entity can be hit.
	 * @return true - if this entity can be hit, false - otherwise.
	 */
	private boolean canHit() {
		return System.currentTimeMillis() - lastHit >= 1000;
	}
	
	/**
	 * Kills this entity.
	 */
	public void kill() {
		lives = 0;
	}
	
	/**
	 * Gets the life amount of this entity.
	 * @return the life amount.
	 */
	public int getLives() {
		return lives;
	}

}