package deipop.underworld.game.level;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.objects.EllipseMapObject;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapRenderer;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer.Cell;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Ellipse;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.ChainShape;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.MassData;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.Timer.Task;

import deipop.underworld.game.Application;
import deipop.underworld.game.constant.Box2D;
import deipop.underworld.game.level.entity.CharacterEntity;
import deipop.underworld.game.level.entity.Entity;
import deipop.underworld.game.level.entity.monster.impl.Bouncer;
import deipop.underworld.game.level.entity.monster.impl.Fly;
import deipop.underworld.game.level.entity.monster.impl.Slime;
import deipop.underworld.game.level.entity.object.Object;
import deipop.underworld.game.level.entity.object.Type;
import deipop.underworld.game.level.entity.object.impl.BlueLock;
import deipop.underworld.game.level.entity.object.impl.Door;
import deipop.underworld.game.level.entity.object.impl.GreenLock;
import deipop.underworld.game.level.entity.object.impl.GroundTile;
import deipop.underworld.game.level.entity.object.impl.Lock;
import deipop.underworld.game.level.entity.object.impl.RedLock;
import deipop.underworld.game.level.entity.object.impl.WaterTile;
import deipop.underworld.game.level.entity.object.impl.YellowLock;
import deipop.underworld.game.level.entity.object.item.Item;
import deipop.underworld.game.level.entity.object.item.impl.BlueKey;
import deipop.underworld.game.level.entity.object.item.impl.BronzeCoin;
import deipop.underworld.game.level.entity.object.item.impl.GoldCoin;
import deipop.underworld.game.level.entity.object.item.impl.GreenKey;
import deipop.underworld.game.level.entity.object.item.impl.RedKey;
import deipop.underworld.game.level.entity.object.item.impl.SilverCoin;
import deipop.underworld.game.level.entity.object.item.impl.YellowKey;
import deipop.underworld.game.level.entity.player.Player;
import deipop.underworld.game.level.entity.player.PlayerFoot;
import deipop.underworld.game.res.Maps;
import deipop.underworld.game.screen.impl.GameEndScreen;
import deipop.underworld.game.screen.impl.GameScreen;
import deipop.underworld.game.util.Camera;

/**
 * @author Deividas Popelskis <deividas.popelskis@gmail.com>
 */
public class Level {
	
	/**
	 * The width of the level.
	 */
	public static final int WIDTH = 96 * 64;
	
	/**
	 * The height of the level.
	 */
	public static final int HEIGHT = 16 * 64;
	
	/**
	 * The level number.
	 */
	private int level;
	
	/**
	 * The level camera.
	 */
	public final Camera camera;
	
	/**
	 * The level debug camera.
	 */
	public final Camera debugCamera;
	
	/**
	 * The map of this level.
	 */
	private final TiledMap map;
	
	/**
	 * The map renderer.
	 */
	private final TiledMapRenderer mapRenderer;
	
	/**
	 * The box2d world.
	 */
	private final World world;
	
	/**
	 * The box2d world renderer.
	 */
	private final Box2DDebugRenderer worldRenderer;
	
	/**
	 * The player.
	 */
	private Player player;
	
	/**
	 * The object list.
	 */
	private List<Object> objects = new ArrayList<>();
	
	/**
	 * The monster list.
	 */
	private List<CharacterEntity> monsters = new ArrayList<>();
	
	/**
	 * The door.
	 */
	private Door door;
	
	/**
	 * The amount of keys.
	 */
	private int keyAmount;
	
	/**
	 * The amount of coins.
	 */
	private int coinAmount;
	
	/**
	 * The sprite batch.
	 */
	private final SpriteBatch spriteBatch = Application.getSpriteBatch();
	
	/**
	 * Level ended flag.
	 */
	private boolean ended;
	
	/**
	 * Creates a new level.
	 * @param lvl - the level number.
	 */
	public Level(int lvl) {
		this.level = lvl;
		
		camera = new Camera();
		camera.setToOrtho(false, Application.WIDTH, Application.HEIGHT);
		
		debugCamera = new Camera(true);
		debugCamera.setToOrtho(false, Application.WIDTH / Box2D.PPM, Application.HEIGHT / Box2D.PPM);
		
		map = Maps.getMap(lvl);
		mapRenderer = new OrthogonalTiledMapRenderer(map);
		
		world = new World(new Vector2(0, Box2D.GRAVITY), true);
		world.setContactListener(new ContactListener(this));
		worldRenderer = new Box2DDebugRenderer();
		
		createPlayer();
		createMonsters();
		createGroundTiles();
		createWaterTiles();
		
		createDoor();
		createLocks();
		createCoins();
		createKeys();
	}
	
	/**
	 * Creates a new level, with saved points from previous level.
	 * @param lvl - the level number.
	 */
	public Level(int points, int lvl) {
		this(lvl);
		
		player.addPoints(points);
	}
	
	/**
	 * Creates the player.
	 */
	private void createPlayer() {
		BodyDef bodyDefinition = new BodyDef();
		bodyDefinition.fixedRotation = true;
		bodyDefinition.type = BodyType.DynamicBody;
		bodyDefinition.position.set(Player.STARTING_POSITION_X / Box2D.PPM, Player.STARTING_POSITION_Y / Box2D.PPM);
		
		PolygonShape bodyShape = new PolygonShape();
		bodyShape.setAsBox(Player.WIDTH / 2 / Box2D.PPM - 0.05f, Player.HEIGHT / 2 / Box2D.PPM);
		
		FixtureDef bodyFixtureDefinition = new FixtureDef();
		bodyFixtureDefinition.friction = 0f;
		bodyFixtureDefinition.shape = bodyShape;
		bodyFixtureDefinition.filter.categoryBits = Box2D.BIT_PLAYER;
		bodyFixtureDefinition.filter.maskBits = Box2D.BIT_GROUND_TILE | Box2D.BIT_OBJECT_TILE | Box2D.BIT_COIN | Box2D.BIT_KEY | Box2D.BIT_DOOR | Box2D.BIT_MONSTER;
		
		PolygonShape footShape = new PolygonShape();
		footShape.setAsBox(Player.WIDTH / 2 / 1.3f / Box2D.PPM, 81 / 2 / 4 / Box2D.PPM, new Vector2(0, -45 / Box2D.PPM), 0f);
		
		FixtureDef footFixtureDefinition = new FixtureDef();
		footFixtureDefinition.isSensor = true;
		footFixtureDefinition.shape = footShape;
		footFixtureDefinition.filter.categoryBits = Box2D.BIT_PLAYER;
		footFixtureDefinition.filter.maskBits = Box2D.BIT_GROUND_TILE | Box2D.BIT_WATER_TILE | Box2D.BIT_MONSTER;
		
		Body body = world.createBody(bodyDefinition);
		
		MassData massData = body.getMassData();
		massData.mass = Player.MASS;
		body.setMassData(massData);
		
		body.createFixture(bodyFixtureDefinition);
		body.createFixture(footFixtureDefinition).setUserData(new PlayerFoot(body));
		
		player = new Player(body);
		body.setUserData(player);
	}
	
	/**
	 * Creates the monsters.
	 */
	private void createMonsters() {
		for (MapObject object : map.getLayers().get("slimes").getObjects()) {
			RectangleMapObject mapObject = (RectangleMapObject) object;
			Rectangle rectangle = mapObject.getRectangle();
			
			BodyDef bodyDef = new BodyDef();
			bodyDef.type = BodyType.KinematicBody;
			bodyDef.position.set((rectangle.x + rectangle.width / 2) / Box2D.PPM, (rectangle.y + rectangle.height / 2) / Box2D.PPM);
			
			PolygonShape shape = new PolygonShape();
			shape.setAsBox(rectangle.width / 2 / Box2D.PPM, rectangle.height / 2 / Box2D.PPM);
			
			FixtureDef fixtureDef = new FixtureDef();
			fixtureDef.shape = shape;
			fixtureDef.friction = 100f;
			fixtureDef.filter.categoryBits = Box2D.BIT_MONSTER;
			fixtureDef.filter.maskBits = Box2D.BIT_PLAYER | Box2D.BIT_GROUND_TILE;
			
			Body body = world.createBody(bodyDef);
			
			Slime slime = new Slime(level, body);

			body.setUserData(slime);
			body.createFixture(fixtureDef);
			
			MassData massData = body.getMassData();
			massData.mass = Player.MASS * 1000;
			body.setMassData(massData);
			
			monsters.add(slime);
		}
		
		for (MapObject object : map.getLayers().get("flies").getObjects()) {
			RectangleMapObject mapObject = (RectangleMapObject) object;
			Rectangle rectangle = mapObject.getRectangle();
			
			BodyDef bodyDef = new BodyDef();
			bodyDef.type = BodyType.KinematicBody;
			bodyDef.position.set((rectangle.x + rectangle.width / 2) / Box2D.PPM, (rectangle.y + rectangle.height / 2) / Box2D.PPM);
			
			PolygonShape shape = new PolygonShape();
			shape.setAsBox(rectangle.width / 2 / Box2D.PPM, rectangle.height / 2 / Box2D.PPM);
			
			FixtureDef fixtureDef = new FixtureDef();
			fixtureDef.shape = shape;
			fixtureDef.friction = 100f;
			fixtureDef.filter.categoryBits = Box2D.BIT_MONSTER;
			fixtureDef.filter.maskBits = Box2D.BIT_PLAYER | Box2D.BIT_GROUND_TILE;
			
			Body body = world.createBody(bodyDef);
			
			Fly fly = new Fly(level, body);

			body.setUserData(fly);
			body.createFixture(fixtureDef);
			
			MassData massData = body.getMassData();
			massData.mass = Player.MASS * 1000;
			body.setMassData(massData);
			
			monsters.add(fly);
		}
		
		for (MapObject object : map.getLayers().get("bouncers").getObjects()) {
			RectangleMapObject mapObject = (RectangleMapObject) object;
			Rectangle rectangle = mapObject.getRectangle();
			
			BodyDef bodyDef = new BodyDef();
			bodyDef.type = BodyType.KinematicBody;
			bodyDef.position.set((rectangle.x + rectangle.width / 2) / Box2D.PPM, (rectangle.y + rectangle.height / 2) / Box2D.PPM);
			
			PolygonShape shape = new PolygonShape();
			shape.setAsBox(rectangle.width / 2 / Box2D.PPM, rectangle.height / 2 / Box2D.PPM);
			
			FixtureDef fixtureDef = new FixtureDef();
			fixtureDef.shape = shape;
			fixtureDef.restitution = 1f;
			fixtureDef.filter.categoryBits = Box2D.BIT_MONSTER;
			fixtureDef.filter.maskBits = Box2D.BIT_PLAYER | Box2D.BIT_GROUND_TILE;
			
			Body body = world.createBody(bodyDef);
			
			Bouncer bouncer = new Bouncer(level, body);

			body.setUserData(bouncer);
			body.createFixture(fixtureDef);
			
			MassData massData = body.getMassData();
			massData.mass = Player.MASS * 1000;
			body.setMassData(massData);
			
			monsters.add(bouncer);
		}
	}
	
	/**
	 * Creates the ground tiles.
	 */
	private void createGroundTiles() {
		TiledMapTileLayer tileLayer = (TiledMapTileLayer) map.getLayers().get("ground_tiles");
		
		for (int x = 0; x < tileLayer.getWidth(); x++) {
			for (int y = 0; y < tileLayer.getHeight(); y++) {
				Cell cell = tileLayer.getCell(x, y);
				
				if (cell != null) {
					BodyDef bodyDef = new BodyDef();
					bodyDef.type = BodyType.StaticBody;
					bodyDef.position.set(x * 64 / Box2D.PPM, y * 64 / Box2D.PPM);
					
					ChainShape shape = new ChainShape();
					
					Vector2[] vertices = new Vector2[5];
					vertices[0] = new Vector2(0 / Box2D.PPM, 0 / Box2D.PPM);
					vertices[1] = new Vector2(64 / Box2D.PPM, 0 / Box2D.PPM);
					vertices[2] = new Vector2(64 / Box2D.PPM, 64 / Box2D.PPM);
					vertices[3] = new Vector2(0 / Box2D.PPM, 64 / Box2D.PPM);
					vertices[4] = new Vector2(0 / Box2D.PPM, 0 / Box2D.PPM);
					
					shape.createChain(vertices);
					
					FixtureDef fixtureDef = new FixtureDef();
					fixtureDef.shape = shape;
					fixtureDef.filter.categoryBits = Box2D.BIT_GROUND_TILE;
					fixtureDef.filter.maskBits = Box2D.BIT_PLAYER | Box2D.BIT_COIN | Box2D.BIT_MONSTER;
					
					Body body = world.createBody(bodyDef);
					body.createFixture(fixtureDef);
					body.setUserData(new GroundTile(body));
				}
			}
		}
	}
	
	/**
	 * Creates the water tiles.
	 */
	private void createWaterTiles() {
		TiledMapTileLayer tileLayer = (TiledMapTileLayer) map.getLayers().get("water_tiles");
		
		for (int x = 0; x < tileLayer.getWidth(); x++) {
			for (int y = 0; y < tileLayer.getHeight(); y++) {
				Cell cell = tileLayer.getCell(x, y);
				
				if (cell == null) {
					continue;
				}
				
				BodyDef bodyDef = new BodyDef();
				bodyDef.type = BodyType.StaticBody;
				bodyDef.position.set(x * 64 / Box2D.PPM, y * 64 / Box2D.PPM);
				
				ChainShape shape = new ChainShape();
				
				Vector2[] vertices = new Vector2[5];
				vertices[0] = new Vector2(0 / Box2D.PPM, 0 / Box2D.PPM);
				vertices[1] = new Vector2(64 / Box2D.PPM, 0 / Box2D.PPM);
				vertices[2] = new Vector2(64 / Box2D.PPM, 64 / Box2D.PPM);
				vertices[3] = new Vector2(0 / Box2D.PPM, 64 / Box2D.PPM);
				vertices[4] = new Vector2(0 / Box2D.PPM, 0 / Box2D.PPM);
				
				shape.createChain(vertices);
				
				FixtureDef fixtureDef = new FixtureDef();
				fixtureDef.shape = shape;
				fixtureDef.filter.categoryBits = Box2D.BIT_WATER_TILE;
				fixtureDef.filter.maskBits = Box2D.BIT_PLAYER;
				
				Body body = world.createBody(bodyDef);
				body.createFixture(fixtureDef);
				body.setUserData(new WaterTile(body));
			}
		}
	}
	
	/**
	 * Creates the door.
	 */
	private void createDoor() {
		for (MapObject object : map.getLayers().get("door").getObjects()) {
			RectangleMapObject mapObject = (RectangleMapObject) object;
			Rectangle rectangle = mapObject.getRectangle();
			
			BodyDef bodyDef = new BodyDef();
			bodyDef.type = BodyType.StaticBody;
			bodyDef.position.set((rectangle.x + rectangle.width / 2) / Box2D.PPM, (rectangle.y + rectangle.height / 2) / Box2D.PPM);
			
			PolygonShape shape = new PolygonShape();
			shape.setAsBox(rectangle.width / 2 / Box2D.PPM, rectangle.height / 2 / Box2D.PPM);
			
			FixtureDef fixtureDef = new FixtureDef();
			fixtureDef.shape = shape;
			fixtureDef.filter.categoryBits = Box2D.BIT_DOOR;
			fixtureDef.filter.maskBits = Box2D.BIT_PLAYER;
			
			Body body = world.createBody(bodyDef);
			
			door = new Door(body);
			
			body.setUserData(door);
			body.createFixture(fixtureDef);
			
			objects.add(door);
		}
	}

	/**
	 * Creates the locks.
	 */
	private void createLocks() {
		String[] keyColors = new String[] { "yellow", "green", "blue", "red" };

		for (String keyColor : keyColors) {
			for (MapObject object : map.getLayers().get(keyColor + "_locks").getObjects())  {
				RectangleMapObject mapObject = (RectangleMapObject) object;
				Rectangle rectangle = mapObject.getRectangle();
				
				BodyDef bodyDef = new BodyDef();
				bodyDef.type = BodyType.StaticBody;
				bodyDef.position.set((rectangle.x + rectangle.width / 2) / Box2D.PPM, rectangle.y / Box2D.PPM);
				
				PolygonShape shape = new PolygonShape();
				shape.setAsBox(rectangle.width / 2 / Box2D.PPM, rectangle.height / 2 / Box2D.PPM);
				
				FixtureDef fixtureDef = new FixtureDef();
				fixtureDef.shape = shape;
				fixtureDef.filter.categoryBits = Box2D.BIT_KEY;
				fixtureDef.filter.maskBits = Box2D.BIT_PLAYER;
				
				Body body = world.createBody(bodyDef);
				
				Lock lock = null;
				
				switch (keyColor) {
				case "yellow":
					lock = new YellowLock(body);
					break;
				case "green":
					lock = new GreenLock(body);
					break;
				case "blue":
					lock = new BlueLock(body);
					break;
				case "red":
					lock = new RedLock(body);
					break;
				}
				
				body.setUserData(lock);
				body.createFixture(fixtureDef);
				
				objects.add(lock);
			}
		}
	}
	
	/**
	 * Creates the coins.
	 */
	private void createCoins() {
		String[] coinMaterials = new String[] { "bronze", "silver", "gold" };
		
		for (String coinMaterial : coinMaterials) {
			for (MapObject object : map.getLayers().get(coinMaterial + "_coins").getObjects()) {
				EllipseMapObject mapObject = (EllipseMapObject) object;
				Ellipse ellipse = mapObject.getEllipse();
				
				BodyDef bodyDef = new BodyDef();
				bodyDef.type = BodyType.DynamicBody;
				bodyDef.position.set((ellipse.x + ellipse.width / 2) / Box2D.PPM, ellipse.y / Box2D.PPM);
				
				CircleShape shape = new CircleShape();
				shape.setRadius(ellipse.width / 2 / Box2D.PPM);
				
				FixtureDef fixtureDef = new FixtureDef();
				fixtureDef.shape = shape;
				fixtureDef.restitution = 1f;
				fixtureDef.filter.categoryBits = Box2D.BIT_COIN;
				fixtureDef.filter.maskBits = Box2D.BIT_GROUND_TILE | Box2D.BIT_PLAYER;
				
				Body body = world.createBody(bodyDef);
				
				Item coin = null;
				
				switch (coinMaterial) {
				case "bronze":
					coin = new BronzeCoin(body);
					break;
				case "silver":
					coin = new SilverCoin(body);
					break;
				case "gold":
					coin = new GoldCoin(body);
					break;
				}
				
				body.setUserData(coin);
				body.createFixture(fixtureDef);
				
				objects.add(coin);
				coinAmount++;
			}
		}
	}
	
	/**
	 * Creates the keys.
	 */
	private void createKeys() {
		String[] keyColors = new String[] { "yellow", "green", "blue", "red" };
		
		for (String keyColor : keyColors) {
			for (MapObject object : map.getLayers().get(keyColor + "_keys").getObjects())  {
				RectangleMapObject mapObject = (RectangleMapObject) object;
				Rectangle rectangle = mapObject.getRectangle();
				
				BodyDef bodyDef = new BodyDef();
				bodyDef.type = BodyType.StaticBody;
				bodyDef.position.set((rectangle.x + rectangle.width / 2) / Box2D.PPM, rectangle.y / Box2D.PPM);
				
				PolygonShape shape = new PolygonShape();
				shape.setAsBox(rectangle.width / 2 / Box2D.PPM, rectangle.height / 2 / Box2D.PPM);
				
				FixtureDef fixtureDef = new FixtureDef();
				fixtureDef.shape = shape;
				fixtureDef.filter.categoryBits = Box2D.BIT_KEY;
				fixtureDef.filter.maskBits = Box2D.BIT_PLAYER;
				
				Body body = world.createBody(bodyDef);
				
				Item key = null;
				
				switch (keyColor) {
				case "yellow":
					key = new YellowKey(body);
					break;
				case "green":
					key = new GreenKey(body);
					break;
				case "blue":
					key = new BlueKey(body);
					break;
				case "red":
					key = new RedKey(body);
					break;
				}
				
				body.setUserData(key);
				body.createFixture(fixtureDef);
				
				objects.add(key);
				keyAmount++;
			}
		}
	}
	
	/**
	 * Updates the level.
	 * @param deltaTime - the delta time.
	 */
	public void update(float deltaTime) {
		Iterator<Object> $it  = objects.iterator();
		
		while ($it.hasNext()) {
			Object object = $it.next();
			
			if (object.isDestroyed()) {
				world.destroyBody(object.getBody());
				$it.remove();
			}
		}
		
		Iterator<CharacterEntity> $mit  = monsters.iterator();
		
		while ($mit.hasNext()) {
			CharacterEntity monster = $mit.next();
			
			if (monster.isDestroyed()) {
				world.destroyBody(monster.getBody());
				$mit.remove();
			}
		}
		
		player.update(deltaTime);
		
		for (CharacterEntity monster : monsters) {
			monster.update(deltaTime);
		}
		
		world.step(1 / 60f, 5, 10);
		
		camera.follow(player);
		camera.update();
		
		spriteBatch.setProjectionMatrix(camera.combined);
		
		if (Application.DEBUG) {
			debugCamera.follow(player);
			debugCamera.update();
		}
	}
	
	/**
	 * Renders the level.
	 */
	public void render() {
		mapRenderer.setView(camera);
		mapRenderer.render();
		
		if (Application.DEBUG) {
			worldRenderer.render(world, debugCamera.combined);
		}
		
		spriteBatch.begin();
		
		player.render(spriteBatch);
		
		for (Entity entity : objects) {
			entity.render(spriteBatch);
		}
		
		for (CharacterEntity monster : monsters) {
			monster.render(spriteBatch);
		}
		
		spriteBatch.end();
	}
	
	/**
	 * Disposes the level.
	 */
	public void dispose() {
		world.dispose();
		worldRenderer.dispose();
	}
	
	/**
	 * Ends the level.
	 */
	public void end() {
		if (ended) {
			return;
		}
		
		ended = true;
		
		Level lvl = this;
		
		player.kill();
		
		Timer.schedule(new Task() {

			@Override
			public void run() {
				Application.setScreen(new GameEndScreen(lvl, false));
			}
			
		}, 0.5f);
	}
	
	/**
	 * Advances to the next level.
	 */
	public void advance() {
		if (level + 1 > Maps.getMapCount()) {
			Application.setScreen(new GameEndScreen(this, true));
		} else {
			Application.setScreen(new GameScreen(player.getPoints(), level + 1));
		}
	}
	
	/**
	 * Updates the door.
	 */
	public void updateDoor() {
		if (canOpenDoor() && !door.isOpen()) {
			door.open();
		}
	}
	
	/**
	 * Checks if the door can be opened.
	 * @return true - if the door can be opened, else - false.
	 */
	public boolean canOpenDoor() {
		return getObjectAmount(Type.YELLOW_LOCK) == 0 && getObjectAmount(Type.GREEN_LOCK) == 0 && getObjectAmount(Type.BLUE_LOCK) == 0 && getObjectAmount(Type.RED_LOCK) == 0;
	}
	
	/**
	 * Gets the door.
	 * @return the door.
	 */
	public Door getDoor() {
		return door;
	}
	
	/**
	 * Gets the object amount.
	 * @param objectType - the object type.
	 * @return the object amount.
	 */
	public int getObjectAmount(Type objectType) {
		int amount = 0;
		
		for (Object object : objects) {
			if (object.getType() == objectType) {
				amount++;
			}
		}
		
		return amount;
	}
	
	/**
	 * Gets the key amount.
	 * @return the key amount.
	 */
	public int getKeyAmount() {
		return keyAmount;
	}
	
	/**
	 * Gets the coin amount.
	 * @return the coin amount.
	 */
	public int getCoinAmount() {
		return coinAmount;
	}
	
	/**
	 * Gets the player.
	 * @return the player.
	 */
	public Player getPlayer() {
		return player;
	}
	
}