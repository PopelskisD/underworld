package deipop.underworld.game.level;

import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.Fixture;

import deipop.underworld.game.level.entity.Entity;
import deipop.underworld.game.util.ContactAdapter;

/**
 * @author Deividas Popelskis <deividas.popelskis@gmail.com>
 */
public class ContactListener extends ContactAdapter {
	
	/**
	 * The level.
	 */
	private final Level level;
	
	/**
	 * Creates a new contact listener.
	 * @param level - the level to create this contact listener for.
	 */
	public ContactListener(Level level) {
		this.level = level;
	}
	
	@Override
	public void beginContact(Contact contact) {
		Fixture fixtureA = contact.getFixtureA();
		Fixture fixtureB = contact.getFixtureB();
		
		Entity firstEntity = null;
		Entity secondEntity = null;
		
		if (fixtureB.getBody().getUserData() instanceof Entity) {
			firstEntity = (Entity) fixtureA.getBody().getUserData();
		}
		
		if (fixtureB.getBody().getUserData() instanceof Entity) {
			secondEntity = (Entity) fixtureB.getBody().getUserData();
		}
		
		if (fixtureA.getUserData() != null) {
			firstEntity = (Entity) fixtureA.getUserData();
		}
		
		if (fixtureB.getUserData() != null) {
			secondEntity = (Entity) fixtureB.getUserData();
		}
		
		if (firstEntity == null || secondEntity == null) {
			return;
		}
		
		firstEntity.onCollisionStart(level, secondEntity);
		secondEntity.onCollisionStart(level, firstEntity);
	}
	
	@Override
	public void endContact(Contact contact) {
		Fixture fixtureA = contact.getFixtureA();
		Fixture fixtureB = contact.getFixtureB();
		
		Entity firstEntity = null;
		Entity secondEntity = null;
		
		if (fixtureB.getBody().getUserData() instanceof Entity) {
			firstEntity = (Entity) fixtureA.getBody().getUserData();
		}
		
		if (fixtureB.getBody().getUserData() instanceof Entity) {
			secondEntity = (Entity) fixtureB.getBody().getUserData();
		}
		
		if (fixtureA.getUserData() != null) {
			firstEntity = (Entity) fixtureA.getUserData();
		}
		
		if (fixtureB.getUserData() != null) {
			secondEntity = (Entity) fixtureB.getUserData();
		}
		
		if (firstEntity == null || secondEntity == null) {
			return;
		}
		
		firstEntity.onCollisionEnd(level, secondEntity);
		secondEntity.onCollisionEnd(level, firstEntity);
	}
	
}
